package pe.certificados.core.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;



public class Ciudadano implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private Integer id;

	private String nombre;
	
	private String apellidoPaterno;
	
	private String apellidoMaterno;

	private String sexo;
	
	private String dni;
	
	private String fechaNacimiento;
	
	private String departamentoNac;
	
	private String provinciaNac;
	
	private String distritoNac;
	
	private String estadoCivil;
	
	private String fechaInscripcion;
	
	private String nombrePadre;
	
	private String nombreMadre;
	
	private String fechaEmision;
	
	private String departamentoDom;
	
	private String provinciaDom;
	
	private String distritoDom;
	
	private String direccion;
	
	private String url;
	
	public Ciudadano() {
	}
	
	public Ciudadano(int id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getDepartamentoNac() {
		return departamentoNac;
	}

	public void setDepartamentoNac(String departamentoNac) {
		this.departamentoNac = departamentoNac;
	}

	public String getProvinciaNac() {
		return provinciaNac;
	}

	public void setProvinciaNac(String provinciaNac) {
		this.provinciaNac = provinciaNac;
	}

	public String getDistritoNac() {
		return distritoNac;
	}

	public void setDistritoNac(String distritoNac) {
		this.distritoNac = distritoNac;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(String fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public String getNombrePadre() {
		return nombrePadre;
	}

	public void setNombrePadre(String nombrePadre) {
		this.nombrePadre = nombrePadre;
	}

	public String getNombreMadre() {
		return nombreMadre;
	}

	public void setNombreMadre(String nombreMadre) {
		this.nombreMadre = nombreMadre;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getDepartamentoDom() {
		return departamentoDom;
	}

	public void setDepartamentoDom(String departamentoDom) {
		this.departamentoDom = departamentoDom;
	}

	public String getDistritoDom() {
		return distritoDom;
	}

	public void setDistritoDom(String distritoDom) {
		this.distritoDom = distritoDom;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getProvinciaDom() {
		return provinciaDom;
	}

	public void setProvinciaDom(String provinciaDom) {
		this.provinciaDom = provinciaDom;
	}

	
}