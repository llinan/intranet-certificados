package pe.certificados.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="precio")
public class Precio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name="precio")
	private Double precio;

	@Column(name="id_tiempo_vigencia")
	private Integer idTiempoVigencia;

	@Column(name="id_tipo_aliado")
	private Integer idTipoAliado;
	
	public Precio() {
	}
	
	public Precio(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdTiempoVigencia() {
		return idTiempoVigencia;
	}

	public void setIdTiempoVigencia(Integer idTiempoVigencia) {
		this.idTiempoVigencia = idTiempoVigencia;
	}

//	public Integer getPrecio() {
//		return precio;
//	}
//
//	public void setPrecio(Integer precio) {
//		this.precio = precio;
//	}
	
	

	public Integer getIdTipoAliado() {
		return idTipoAliado;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public void setIdTipoAliado(Integer idTipoAliado) {
		this.idTipoAliado = idTipoAliado;
	}

}