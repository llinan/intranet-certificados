package pe.certificados.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="parametros_servidor_correo")
public class ParametrosServidorCorreo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(generator = "sq_empresa")
//	@SequenceGenerator(schema = "identifysys", name = "sq_empresa", sequenceName = "sq_empresa", allocationSize = 1)
	private Integer id;

	@Column(name="host")
	private String host;
		
	@Column(name="usuario")
	private String usuario;

	@Column(name="clave")
	private String clave;
	
	@Column(name="starttls_enable")
	private String starttls_enable;
	
	@Column(name="port")
	private Integer port;
	
	@Column(name="socketfactory_port")
	private String socketfactory_port;
	
	@Column(name="socketfactory_class")
	private String socketfactory_class;
	
	@Column(name="ssl_trust")
	private String ssl_trust;
	
	@Column(name="auth")
	private String auth;

	@Column(name="correo_remitente")
	private String correoRemitente;
	
	public ParametrosServidorCorreo() {
	}
	
	public ParametrosServidorCorreo(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getStarttls_enable() {
		return starttls_enable;
	}

	public void setStarttls_enable(String starttls_enable) {
		this.starttls_enable = starttls_enable;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getSocketfactory_port() {
		return socketfactory_port;
	}

	public void setSocketfactory_port(String socketfactory_port) {
		this.socketfactory_port = socketfactory_port;
	}

	public String getSocketfactory_class() {
		return socketfactory_class;
	}

	public void setSocketfactory_class(String socketfactory_class) {
		this.socketfactory_class = socketfactory_class;
	}

	public String getSsl_trust() {
		return ssl_trust;
	}

	public void setSsl_trust(String ssl_trust) {
		this.ssl_trust = ssl_trust;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getCorreoRemitente() {
		return correoRemitente;
	}

	public void setCorreoRemitente(String correoRemitente) {
		this.correoRemitente = correoRemitente;
	}

}