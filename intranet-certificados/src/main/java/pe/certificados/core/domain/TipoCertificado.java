package pe.certificados.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="tipo_certificado")
public class TipoCertificado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(generator = "sq_empresa")
//	@SequenceGenerator(schema = "identifysys", name = "sq_empresa", sequenceName = "sq_empresa", allocationSize = 1)
	private Integer id;

	@Column(name="nombre")
	private String nombre;
		
	@Column(name="estado_registro")
	private Integer estadoRegistro;


	public TipoCertificado() {
	}
	
	public TipoCertificado(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}