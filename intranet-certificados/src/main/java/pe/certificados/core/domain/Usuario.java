package pe.certificados.core.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="usuario")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "sq_usuario")
	@SequenceGenerator(schema = "certificadossys", name = "sq_usuario", sequenceName = "sq_usuario", allocationSize = 1)
	private Integer id;

	private String login;
	
	private String clave;
	
	@Column(name="nombres")
	private String nombres;

	@Column(name="apellidos")
	private String apellidos;

	@Column(name="ruc")
	private String ruc;
	
	@Column(name="dni")
	private String dni;
	
	@Column(name="correo_electronico")
	private String correoElectronico;
	
	@Column(name="telefono")
	private String telefono;
	
	@Column(name="estado_registro")
	private Integer estadoRegistro;
	
	@Column(name="fecha_registro")
	private Date fechaRegistro;
	
	@Column(name="fecha_modificacion")
	private Date fechaModificacion;
	
	@ManyToOne
    @JoinColumn(name = "id_tipo_aliado", nullable = false)
	private TipoAliado tipoAliado;

	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "usuario_opciones",
            joinColumns = { @JoinColumn(name = "id_usuario") },
            inverseJoinColumns = { @JoinColumn(name = "id_opciones") })
	@OrderBy("orden")
    private List<Opciones> opciones;
	
	@Column(name="tipo")
	private String tipo;
	
	@Column(name="flat_cantidad_pedidos")
	private Integer flatCantidadPedidos;
	
	@Column(name="cantidad_pedidos")
	private Integer cantidadPedidos;
		
	@Transient
	private String contexto;

	public Usuario() {
	}
	
	public Usuario(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Integer getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
		
	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getContexto() {
		return contexto;
	}

	public void setContexto(String contexto) {
		this.contexto = contexto;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public void setOpciones(List<Opciones> opciones) {
		this.opciones = opciones;
	}

	public List<Opciones> getOpciones() {
		return opciones;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public TipoAliado getTipoAliado() {
		return tipoAliado;
	}

	public void setTipoAliado(TipoAliado tipoAliado) {
		this.tipoAliado = tipoAliado;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	public Integer getCantidadPedidos() {
		return cantidadPedidos;
	}

	public void setCantidadPedidos(Integer cantidadPedidos) {
		this.cantidadPedidos = cantidadPedidos;
	}

	public Integer getFlatCantidadPedidos() {
		return flatCantidadPedidos;
	}

	public void setFlatCantidadPedidos(Integer flatCantidadPedidos) {
		this.flatCantidadPedidos = flatCantidadPedidos;
	}
	
}