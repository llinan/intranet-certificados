package pe.certificados.core.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name="opciones")
public class Opciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	private String accion;

	@Column(name="codigo_opcion")
	private String codigoOpcion;

	@Column(name="codigo_opcion_padre")
	private String codigoOpcionPadre;

	@Column(name="estado_registro")
	private Integer estadoRegistro;

	@Column(name="icono_class")
	private String iconoClass;

	private Integer nivel;

	private String nombre;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "opciones")
    private List<Usuario> usuarios;

	@Column(name="tipo")
	private String tipo;
	
	public Opciones() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getCodigoOpcion() {
		return this.codigoOpcion;
	}

	public void setCodigoOpcion(String codigoOpcion) {
		this.codigoOpcion = codigoOpcion;
	}

	public String getCodigoOpcionPadre() {
		return this.codigoOpcionPadre;
	}

	public void setCodigoOpcionPadre(String codigoOpcionPadre) {
		this.codigoOpcionPadre = codigoOpcionPadre;
	}

	public Integer getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getIconoClass() {
		return this.iconoClass;
	}

	public void setIconoClass(String iconoClass) {
		this.iconoClass = iconoClass;
	}

	public Integer getNivel() {
		return this.nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	

}