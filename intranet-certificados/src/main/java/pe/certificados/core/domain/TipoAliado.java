package pe.certificados.core.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="tipo_aliado")
public class TipoAliado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(generator = "sq_empresa")
//	@SequenceGenerator(schema = "identifysys", name = "sq_empresa", sequenceName = "sq_empresa", allocationSize = 1)
	private Integer id;

	@Column(name="nombre")
	private String nombre;
		
	@Column(name="minimo")
	private Integer minimo;

	@Column(name="maximo")
	private Integer maximo;
	
	@Column(name="estado_registro")
	private Integer estadoRegistro;

//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "id_empresa", nullable = false)
//	private Empresa empresa;

//	@ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "usuario_opciones",
//            joinColumns = { @JoinColumn(name = "id_usuario") },
//            inverseJoinColumns = { @JoinColumn(name = "id_opciones") })
//	@OrderBy("orden")
//    private List<Opciones> opciones;
	
	@JsonIgnore
	@OneToMany(mappedBy = "tipoAliado")
	private List<Usuario> usuarios;
	
	public TipoAliado() {
	}
	
	public TipoAliado(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getMinimo() {
		return minimo;
	}

	public void setMinimo(Integer minimo) {
		this.minimo = minimo;
	}

	public Integer getMaximo() {
		return maximo;
	}

	public void setMaximo(Integer maximo) {
		this.maximo = maximo;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	

}