package pe.certificados.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="parametros_generales")
public class ParametrosGenerales implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	
	@Column(name="identificador")
	private String identificador;
	
	@Column(name="dato1")
	private String dato1;
	
	@Column(name="dato2")
	private String dato2;
	
	@Column(name="dato3")
	private String dato3;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getDato1() {
		return dato1;
	}

	public void setDato1(String dato1) {
		this.dato1 = dato1;
	}

	public String getDato2() {
		return dato2;
	}

	public void setDato2(String dato2) {
		this.dato2 = dato2;
	}

	public String getDato3() {
		return dato3;
	}

	public void setDato3(String dato3) {
		this.dato3 = dato3;
	}

}
