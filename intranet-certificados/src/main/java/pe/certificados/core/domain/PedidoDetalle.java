package pe.certificados.core.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="pedido_detalle")
public class PedidoDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "sq_pedido_detalle")
	@SequenceGenerator(schema = "certificadossys", name = "sq_pedido_detalle", sequenceName = "sq_pedido_detalle", allocationSize = 1)
	private Integer id;

	@Column(name="ruc")
	private String ruc;

	@Column(name="razon_social")
	private String razonSocial;

	@Column(name="direccion_completa")
	private String direccionCompleta;
	
	@Column(name="pais")
	private String pais;
	
	@Column(name="departamento")
	private String departamento;
	
	@Column(name="provincia")
	private String provincia;
	
	@Column(name="distrito")
	private String distrito;
	
	@Column(name="nombre_representante_legal")
	private String nombreRepresentanteLegal;
	
	@Column(name="dni_representante_legal")
	private String dniRepresentanteLegal;
	
	@Column(name="telefono_representante_legal")
	private String telefonoRepresentanteLegal;
	
	@Column(name="correo_representante_legal")
	private String correoRepresentanteLegal;
	
	@Column(name="ruc_facturacion")
	private String rucFacturacion;
	
	@Column(name="razon_social_facturacion")
	private String razonSocialFacturacion;
	
	@Column(name="direccion_facturacion")
	private String direccionFacturacion;
	
	@ManyToOne
    @JoinColumn(name = "id_pedido", nullable = false)
	private Pedido pedido;
	
	
	public PedidoDetalle() {
	}
	
	public PedidoDetalle(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getDireccionCompleta() {
		return direccionCompleta;
	}

	public void setDireccionCompleta(String direccionCompleta) {
		this.direccionCompleta = direccionCompleta;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getNombreRepresentanteLegal() {
		return nombreRepresentanteLegal;
	}

	public void setNombreRepresentanteLegal(String nombreRepresentanteLegal) {
		this.nombreRepresentanteLegal = nombreRepresentanteLegal;
	}

	public String getDniRepresentanteLegal() {
		return dniRepresentanteLegal;
	}

	public void setDniRepresentanteLegal(String dniRepresentanteLegal) {
		this.dniRepresentanteLegal = dniRepresentanteLegal;
	}

	public String getTelefonoRepresentanteLegal() {
		return telefonoRepresentanteLegal;
	}

	public void setTelefonoRepresentanteLegal(String telefonoRepresentanteLegal) {
		this.telefonoRepresentanteLegal = telefonoRepresentanteLegal;
	}

	public String getCorreoRepresentanteLegal() {
		return correoRepresentanteLegal;
	}

	public void setCorreoRepresentanteLegal(String correoRepresentanteLegal) {
		this.correoRepresentanteLegal = correoRepresentanteLegal;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public String getRucFacturacion() {
		return rucFacturacion;
	}

	public void setRucFacturacion(String rucFacturacion) {
		this.rucFacturacion = rucFacturacion;
	}

	public String getRazonSocialFacturacion() {
		return razonSocialFacturacion;
	}

	public void setRazonSocialFacturacion(String razonSocialFacturacion) {
		this.razonSocialFacturacion = razonSocialFacturacion;
	}

	public String getDireccionFacturacion() {
		return direccionFacturacion;
	}

	public void setDireccionFacturacion(String direccionFacturacion) {
		this.direccionFacturacion = direccionFacturacion;
	}
	
}