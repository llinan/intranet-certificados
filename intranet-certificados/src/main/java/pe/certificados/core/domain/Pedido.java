package pe.certificados.core.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="pedido")
public class Pedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "sq_pedido")
	@SequenceGenerator(schema = "certificadossys", name = "sq_pedido", sequenceName = "sq_pedido", allocationSize = 1)
	private Integer id;

	@Column(name="id_tipo_certificado")
	private Integer idTipoCertificado;

	@Column(name="id_tiempo_vigencia")
	private Integer idTiempoVigencia;

	@Column(name="monto")
	private Double monto;
	
	@Column(name="id_usuario")
	private Integer idUsuario;
	
	@Column(name="fecha_registro")
	private Date fechaRegistro;
	
	@Column(name="numero")
	private Long numero;
	
	@JsonIgnore
	@OneToMany(mappedBy = "pedido")
	private List<PedidoDetalle> pedidosDetalle;
	
	@Column(name="id_estado_pedido")
	private Integer idEstadoPedido;
	
	@Column(name="estado_registro")
	private Integer estadoRegistro;
	
	@Column(name="ruta_imagen")
	private String rutaImagen;

	@Column(name="fecha_pago")
	private Date fechaPago;
	
	public Pedido() {
	}
	
	public Pedido(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdTipoCertificado() {
		return idTipoCertificado;
	}

	public void setIdTipoCertificado(Integer idTipoCertificado) {
		this.idTipoCertificado = idTipoCertificado;
	}

	public Integer getIdTiempoVigencia() {
		return idTiempoVigencia;
	}

	public void setIdTiempoVigencia(Integer idTiempoVigencia) {
		this.idTiempoVigencia = idTiempoVigencia;
	}

//	public Integer getMonto() {
//		return monto;
//	}
//
//	public void setMonto(Integer monto) {
//		this.monto = monto;
//	}
	public Integer getIdUsuario() {
		return idUsuario;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public List<PedidoDetalle> getPedidosDetalle() {
		if(this.pedidosDetalle == null) {
			this.pedidosDetalle = new ArrayList<PedidoDetalle>();
		}
		return pedidosDetalle;
	}

	public void setPedidosDetalle(List<PedidoDetalle> pedidosDetalle) {
		this.pedidosDetalle = pedidosDetalle;
	}

	public Integer getIdEstadoPedido() {
		return idEstadoPedido;
	}

	public void setIdEstadoPedido(Integer idEstadoPedido) {
		this.idEstadoPedido = idEstadoPedido;
	}

	public Integer getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getRutaImagen() {
		return rutaImagen;
	}

	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	
}