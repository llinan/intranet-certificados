package pe.certificados.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.certificados.core.domain.Pedido;
import pe.certificados.core.domain.Precio;


public interface PrecioRepository extends JpaRepository<Pedido, Integer> {

	
	@Query("   SELECT p FROM Precio p " +		
	           "      WHERE p.idTiempoVigencia = :idTiempoVigencia " +
	           "      AND p.idTipoAliado = :idTipoAliado" )
		Precio obtenerPrecioPorTipoAliadoYTiempoVigencia(@Param("idTiempoVigencia") int idTiempoVigencia,
													 @Param("idTipoAliado") int idTipoAliado) throws Exception;
}
