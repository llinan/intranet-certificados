package pe.certificados.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.certificados.core.domain.ParametrosServidorCorreo;
import pe.certificados.core.domain.TipoAliado;
import pe.certificados.core.domain.Usuario;

public interface ParametrosServidorCorreoRepository extends JpaRepository<ParametrosServidorCorreo, Integer> {

}
