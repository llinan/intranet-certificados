package pe.certificados.core.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.certificados.core.domain.PedidoNumero;


public interface PedidoNumeroRepository extends JpaRepository<PedidoNumero, Integer> {

	
	@Transactional
	@Modifying
	@Query("   UPDATE PedidoNumero p " +
	       "      SET p.correlativo = :correlativo" +
		   "    WHERE p.id = :id")
	void actualizarCorrelativo(@Param("id") int id, 
			                   @Param("correlativo") long correlativo) throws Exception;
}
