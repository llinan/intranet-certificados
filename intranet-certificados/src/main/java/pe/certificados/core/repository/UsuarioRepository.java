package pe.certificados.core.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.certificados.core.domain.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Query(value = "SELECT u FROM Usuario u WHERE u.login = :login")
	Usuario buscarPorLogin(@Param("login") String login) throws Exception;
	
	
	@Query(value = " SELECT u FROM Usuario u "
		     + "  WHERE u.estadoRegistro = 1 "
		     + "    AND u.dni = :dni ")
	 Usuario validacionDniUsuario(@Param("dni") String dni) throws Exception;
	
	@Query(value = " SELECT u FROM Usuario u "
		     + "  WHERE u.estadoRegistro = 1 "
		     + "    AND u.ruc = :ruc ")
	 Usuario validacionRucUsuario(@Param("ruc") String ruc) throws Exception;
	
	@Query(value = "SELECT u FROM Usuario u WHERE u.tipo = 'C'")
	List<Usuario> listarUsuarios() throws Exception;

	@Modifying
	@Query(" UPDATE Usuario u SET u.estadoRegistro = :estadoRegistro WHERE u.id = :id " )
	void habilitarInhabilitar(@Param("id") int id, 
						      @Param("estadoRegistro") int estadoRegistro) throws Exception;
	
	
	@Modifying
	@Query(value = "INSERT INTO certificadossys.usuario_opciones (id_usuario, id_opciones) VALUES(:idUsuario, :idOpcion)", nativeQuery = true)
	void adicionarOpcion(@Param("idUsuario") int idUsuario, @Param("idOpcion") int idOpcion) throws Exception;
	
	@Modifying
	@Query(" UPDATE Usuario u SET u.clave = :clave WHERE u.id = :id " )
	void crearClaveUsuario(@Param("id") Integer id, 
						      @Param("clave") String clave) throws Exception;
	
//	@Transactional
//	@Modifying
//	@Query("   UPDATE Usuario u " +
//	       "      SET u.cantidadPedidos = :cantidadPedidos" +
//		   "    WHERE u.id = :id")
//	void actualizarNumeroPedidosPagados(@Param("id") int id, 
//			                   @Param("cantidadPedidos") int cantidadPedidos) throws Exception;
	
	@Transactional
	@Modifying
	@Query("   UPDATE Usuario u " +
	       "      SET u.tipoAliado.id = :idTipoAliado" +
		   "    WHERE u.id = :id")
	void actualizarTipoAliado(@Param("id") int id, 
			                  @Param("idTipoAliado") int idTipoAliado) throws Exception;
	
	@Modifying
	@Query(" UPDATE Usuario u SET u.clave = null WHERE u.id = :id " )
	void limpiarClaveParaGenerarNuevaClave(@Param("id") Integer id) throws Exception;
}
