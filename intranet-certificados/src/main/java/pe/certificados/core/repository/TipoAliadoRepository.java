package pe.certificados.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.certificados.core.domain.TipoAliado;
import pe.certificados.core.domain.Usuario;

public interface TipoAliadoRepository extends JpaRepository<TipoAliado, Integer> {

	
	@Query(value = "SELECT u FROM TipoAliado u WHERE u.estadoRegistro = 1"
					+ "	ORDER BY u.id asc	")
	List<TipoAliado> listarTodos() throws Exception;
}
