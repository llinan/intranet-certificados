package pe.certificados.core.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.certificados.core.domain.Pedido;


public interface PedidoRepository extends JpaRepository<Pedido, Integer> {

	@Query(value = "    SELECT p FROM Pedido p " +
			   	   "     WHERE p.idUsuario = :idUsuario " +
				   "	 AND p.estadoRegistro = 1")
	List<Pedido> obtenerPedidoPorIdUsuario(@Param("idUsuario") int idUsuario) throws Exception;
	
	@Transactional
	@Modifying
	@Query("   UPDATE Pedido p " +
	       "      SET p.idEstadoPedido = :idEstadoPedido" +
		   "    WHERE p.id = :id")
	void actualizarEstadoPedido(@Param("id") int id, 
			              @Param("idEstadoPedido") int idEstadoPedido) throws Exception;
	
	@Transactional
	@Modifying
	@Query("   UPDATE Pedido p " +
	       "      SET p.rutaImagen = :rutaImagen," +
	       "          p.fechaPago = :fechaPago" +
		   "    WHERE p.id = :id")
	void agregarImagenRuta(@Param("id") int id, 
			          @Param("rutaImagen") String rutaImagen,
			          @Param("fechaPago") Date fechaPago) throws Exception;
	
	@Query(value = "    SELECT p FROM Pedido p " +
			       "	 WHERE p.estadoRegistro = 1")
	List<Pedido> listarTodos() throws Exception;
	
	@Query(value = "    SELECT COUNT(1) FROM Pedido p " +
		   	   "     WHERE p.idUsuario = :idUsuario " +
			   "	 AND p.estadoRegistro = 1" +
		   	   "     AND p.idEstadoPedido = 8")
	Integer obtenerPedidosPorUsuarioEstadoPagoConfirmado(@Param("idUsuario") int idUsuario) throws Exception;
}
