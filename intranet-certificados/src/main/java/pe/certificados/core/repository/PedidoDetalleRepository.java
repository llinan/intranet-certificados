package pe.certificados.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.certificados.core.domain.PedidoDetalle;


public interface PedidoDetalleRepository extends JpaRepository<PedidoDetalle, Integer> {

	@Query(value = "    SELECT p FROM PedidoDetalle p " +
			   "     WHERE p.pedido.id = :idPedido ")
	PedidoDetalle obtenerPedidoDetallePorIdPedido(@Param("idPedido") int idPedido) throws Exception;
	
	@Query(value = "    SELECT p FROM PedidoDetalle p " +
			   "     WHERE p.pedido.id = :idPedido ")
	List<PedidoDetalle> listaPedidoDetallePorIdPedido(@Param("idPedido") int idPedido) throws Exception;
}
