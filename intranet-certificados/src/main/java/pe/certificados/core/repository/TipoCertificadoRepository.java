package pe.certificados.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pe.certificados.core.domain.TipoCertificado;

public interface TipoCertificadoRepository extends JpaRepository<TipoCertificado, Integer> {

	@Query(value = "SELECT t FROM TipoCertificado t WHERE t.estadoRegistro = 1")
	List<TipoCertificado> listarRegistrosActivos() throws Exception;
}
