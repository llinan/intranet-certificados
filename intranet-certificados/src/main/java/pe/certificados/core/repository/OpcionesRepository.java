package pe.certificados.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pe.certificados.core.domain.Opciones;


@Repository
public interface OpcionesRepository extends JpaRepository<Opciones, Integer>{

	@Query(value = "SELECT o FROM Opciones o WHERE o.estadoRegistro = 1 AND o.codigoOpcion = :codigo")
	Opciones buscarPorCodigo(@Param("codigo") String codigo) throws Exception;
	
	@Query(value = "SELECT o FROM Opciones o WHERE o.estadoRegistro = 1 AND o.tipo = 'C'")
	List<Opciones> obtenerListaOpcionesParaAliados() throws Exception;
	
	@Query(value = "SELECT o FROM Opciones o WHERE o.estadoRegistro = 1 AND o.tipo = 'A'")
	List<Opciones> obtenerListaOpcionesParaAdmin() throws Exception;
	
	
}
