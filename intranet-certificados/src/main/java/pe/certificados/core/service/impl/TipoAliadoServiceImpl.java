package pe.certificados.core.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.certificados.core.domain.TipoAliado;
import pe.certificados.core.repository.TipoAliadoRepository;
import pe.certificados.core.service.TipoAliadoService;


@Service
public class TipoAliadoServiceImpl implements TipoAliadoService{

	@Autowired
	TipoAliadoRepository tipoAliadoRepository;

	@Override
	public TipoAliado buscarPorId(Integer id) throws Exception {
		return tipoAliadoRepository.findById(id).get();
	}

	@Override
	public List<TipoAliado> listarTodos() throws Exception {
		return tipoAliadoRepository.listarTodos();
	}
	
	
}
