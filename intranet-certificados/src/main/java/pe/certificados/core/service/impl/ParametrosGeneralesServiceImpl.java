package pe.certificados.core.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.certificados.core.domain.ParametrosGenerales;
import pe.certificados.core.repository.ParametrosGeneralesRepository;
import pe.certificados.core.service.ParametrosGeneralesService;


@Service
public class ParametrosGeneralesServiceImpl implements ParametrosGeneralesService{

	@Autowired
	ParametrosGeneralesRepository parametrosGeneralesRepository;

	@Override
	public ParametrosGenerales buscarPorId(Integer id) throws Exception {
		return parametrosGeneralesRepository.findById(id).get();
	}
	
	
}
