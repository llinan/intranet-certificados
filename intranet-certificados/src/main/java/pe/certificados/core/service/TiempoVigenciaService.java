package pe.certificados.core.service;

import java.util.List;

import pe.certificados.core.domain.TiempoVigencia;

public interface TiempoVigenciaService {
	
	List<TiempoVigencia> listarTodos() throws Exception;

	String buscarPorId(Integer id) throws Exception;

}
