package pe.certificados.core.service;


import pe.certificados.core.domain.ParametrosGenerales;

public interface ParametrosGeneralesService {
	
	ParametrosGenerales buscarPorId(Integer id) throws Exception;


}
