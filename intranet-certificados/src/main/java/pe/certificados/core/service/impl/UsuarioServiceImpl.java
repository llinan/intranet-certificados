package pe.certificados.core.service.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.certificados.core.domain.Opciones;
import pe.certificados.core.domain.ParametrosGenerales;
import pe.certificados.core.domain.ParametrosServidorCorreo;
import pe.certificados.core.domain.Usuario;
import pe.certificados.core.repository.OpcionesRepository;
import pe.certificados.core.repository.ParametrosGeneralesRepository;
import pe.certificados.core.repository.ParametrosServidorCorreoRepository;
import pe.certificados.core.repository.PedidoRepository;
import pe.certificados.core.repository.UsuarioRepository;
import pe.certificados.core.service.UsuarioService;
import pe.certificados.core.type.TipoAliadoEnum;
import pe.certificados.web.bean.Email;
import pe.certificados.web.bean.EmailConfig;
import pe.certificados.web.bean.EmailTemplate;
import pe.certificados.web.util.EmailUtil;
import pe.certificados.web.util.EncriptaUtil;


@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	OpcionesRepository opcionesRepository;
	
	@Autowired
	ParametrosServidorCorreoRepository parametrosServidorCorreoRepository;
	
	@Autowired
	ParametrosGeneralesRepository parametrosGeneralesRepository;
	
	@Autowired
	PedidoRepository pedidoRepository;
	
	@Override
	public List<Usuario> listarTodos() throws Exception {
		return usuarioRepository.listarUsuarios();
	}
	
	@Override
	@Transactional
	public Usuario guardar(Usuario usuario) throws Exception{
		
//		if( usuario.getId() != null && (usuario.getClave()==null || "".equals(usuario.getClave()) ) ) {
//			
//			usuario = usuarioRepository.findById(usuario.getId()).get();
//			usuario.setLogin(usuario.getLogin());
//			return usuarioRepository.save(usuario);
//			
//		}
		usuario = mantenerClaveActualSiClaveActualizarEsVacio(usuario);
		
		Usuario usuarios = usuarioRepository.save(usuario);

		List<Opciones> listaOpciones = null;
		
		if(usuarios.getTipo().equals("A")){
			listaOpciones = opcionesRepository.obtenerListaOpcionesParaAdmin();	
		}else{
			listaOpciones =  opcionesRepository.obtenerListaOpcionesParaAliados();
		}

		for(Opciones opciones : listaOpciones){
			usuarioRepository.adicionarOpcion(usuarios.getId(), opciones.getId());
		}
		
		return usuarios;
	}
	
	@Override
	public Usuario buscarPorId(Integer id) throws Exception{
		Usuario usuario = usuarioRepository.findById(id).get();
		return usuario;
	}
	
	public Usuario iniciarSesion(String login, String clave) throws Exception{
		Usuario usuario = usuarioRepository.buscarPorLogin(login.toLowerCase());
		if( usuario!=null && usuario.getClave().equals(clave) ) {
			return usuario;
		}
		return null;
	}

	@Override
	public Usuario validacionDniUsuario(String login) throws Exception {
		return usuarioRepository.validacionDniUsuario(login);
	}

	@Override
	public void eliminar(Integer id) throws Exception {
		 usuarioRepository.deleteById(id);		
	}

	@Override
	@Transactional
	public void habilitarInhabilitar(Integer id, Integer estadoRegistro) throws Exception {
		usuarioRepository.habilitarInhabilitar(id, estadoRegistro);
	}

	@Override
	public void enviarNotificacionCorreoElectronico(Usuario usuario) throws Exception {

		ParametrosServidorCorreo parametrosServidorCorreo = parametrosServidorCorreoRepository.findById(1).get();
		
		ParametrosGenerales parametrosGenerales = parametrosGeneralesRepository.findById(1).get();
		
		ParametrosGenerales parametrosGeneralesDireccionUrl = parametrosGeneralesRepository.findById(3).get();

		
		String plantillaNotifacion = obtenerNotificacionHtmlDesdeArchivo(parametrosGenerales.getDato1());
		
		String idUsuarioEncriptado = EncriptaUtil.encriptarIdUsuario(String.valueOf(usuario.getId()));

		
		String urlIdUsuario = parametrosGeneralesDireccionUrl.getDato1() + "login?c=" +  idUsuarioEncriptado;

			
		Map<String, Object> mapaInformacionCorreo = new HashMap<String, Object>();
		mapaInformacionCorreo.put("NOMBRE", usuario.getNombres()+" "+usuario.getApellidos());
		mapaInformacionCorreo.put("CORREOELECTRONICO", usuario.getLogin());	
		mapaInformacionCorreo.put("url",urlIdUsuario);

		
		EmailConfig emailConfig = new EmailConfig();
		emailConfig.setSmtpHost(parametrosServidorCorreo.getHost());
		emailConfig.setSmtpAuth(parametrosServidorCorreo.getAuth());
		emailConfig.setSmtpUser(parametrosServidorCorreo.getUsuario());
		emailConfig.setSmtpPassword(parametrosServidorCorreo.getClave());
		emailConfig.setSmtpStartTlsEnable(parametrosServidorCorreo.getStarttls_enable());
		emailConfig.setSmtpPort(parametrosServidorCorreo.getPort());
		emailConfig.setSmtpSslTrust(parametrosServidorCorreo.getSsl_trust());
		
		List<Email> listaEmail = new ArrayList<>();
		Email email = new Email();
		email.setEmailFrom(parametrosServidorCorreo.getCorreoRemitente());
		email.setEmailTo(usuario.getCorreoElectronico());
		email.setEmailSubject("BIENVENIDO A INTRANET-CERTIFICADOS");
		email.setEmailBody(EmailUtil.reemplazarValoresPlantillaHtmlSolicitudCambioCorreo(plantillaNotifacion,mapaInformacionCorreo));
		listaEmail.add(email);
		
		EmailTemplate emailTemplate = new EmailTemplate();
		emailTemplate.setEmailConfig(emailConfig);
		emailTemplate.setListaEmail(listaEmail);
		
		EmailUtil.send(emailTemplate);
		
	}

	/**Llinan**/
	
	private String obtenerNotificacionHtmlDesdeArchivo(String ruta) throws Exception{
		FileReader fr = null;
		BufferedReader br = null;
		StringBuilder content=new StringBuilder(1024);
		try {
			fr = new FileReader(ruta);
			br = new BufferedReader(fr);
			String s=null;
			while((s=br.readLine())!=null){
			    content.append(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(br!=null)
				br.close();
			if(fr!=null)
				fr.close();
		}
		return content.toString();
	}

	@Override
	public String obtenerNombreApellidoUsuario(Integer id) throws Exception {
		Usuario usuario = usuarioRepository.findById(id).get();
		return usuario.getNombres() + " " + usuario.getApellidos();
	}

	@Override
	@Transactional
	public void crearClaveUsuario(Integer id, String clave) throws Exception {
		 usuarioRepository.crearClaveUsuario(id, clave);
	}

	@Override
	public void actualizarNumeroPedidosPagados(Integer id) throws Exception {
		Usuario usuario = usuarioRepository.findById(id).get();
		Integer nuevacantidadPedidos = pedidoRepository.obtenerPedidosPorUsuarioEstadoPagoConfirmado(usuario.getId());
//		usuarioRepository.actualizarNumeroPedidosPagados(usuario.getId(), nuevacantidadPedidos);
		
		if(usuario.getFlatCantidadPedidos() != null ) {
			System.out.println("[ActualizarNumeroPedidosPagados] - Flat Cantidad Pedidos");
			nuevacantidadPedidos = nuevacantidadPedidos + usuario.getCantidadPedidos();
			if(nuevacantidadPedidos >= TipoAliadoEnum.BASIC.getMinimo() && nuevacantidadPedidos <= TipoAliadoEnum.BASIC.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.BASIC.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : BASIC" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos >= TipoAliadoEnum.BRONCE.getMinimo() && nuevacantidadPedidos <= TipoAliadoEnum.BRONCE.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.BRONCE.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : BRONCE" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos >= TipoAliadoEnum.PLATA.getMinimo() && nuevacantidadPedidos <= TipoAliadoEnum.PLATA.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.PLATA.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : PLATA" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos >= TipoAliadoEnum.ORO.getMinimo() && nuevacantidadPedidos <= TipoAliadoEnum.ORO.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.ORO.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : ORO" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos >= TipoAliadoEnum.PLATINIUM.getMinimo() && nuevacantidadPedidos <= TipoAliadoEnum.PLATINIUM.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.PLATINIUM.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : PLATINIUM" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos >= TipoAliadoEnum.VIP.getMinimo() && nuevacantidadPedidos <= TipoAliadoEnum.VIP.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.VIP.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : VIP" + " + Usuario : " + usuario.getId());
			}
		}else{
			if(nuevacantidadPedidos+1 >= TipoAliadoEnum.BASIC.getMinimo() && nuevacantidadPedidos+1 <= TipoAliadoEnum.BASIC.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.BASIC.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : BASIC" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos+1 >= TipoAliadoEnum.BRONCE.getMinimo() && nuevacantidadPedidos+1 <= TipoAliadoEnum.BRONCE.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.BRONCE.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : BRONCE" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos+1 >= TipoAliadoEnum.PLATA.getMinimo() && nuevacantidadPedidos+1 <= TipoAliadoEnum.PLATA.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.PLATA.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : PLATA" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos+1 >= TipoAliadoEnum.ORO.getMinimo() && nuevacantidadPedidos+1 <= TipoAliadoEnum.ORO.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.ORO.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : ORO" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos+1 >= TipoAliadoEnum.PLATINIUM.getMinimo() && nuevacantidadPedidos+1 <= TipoAliadoEnum.PLATINIUM.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.PLATINIUM.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : PLATINIUM" + " + Usuario : " + usuario.getId());
			}else if(nuevacantidadPedidos+1 >= TipoAliadoEnum.VIP.getMinimo() && nuevacantidadPedidos+1 <= TipoAliadoEnum.VIP.getMaximo()){
				usuarioRepository.actualizarTipoAliado(usuario.getId(), TipoAliadoEnum.VIP.getId());
				System.out.println("[ActualizarNumeroPedidosPagados] + Tipo Aliado : VIP" + " + Usuario : " + usuario.getId());
			}
		}	
	}

	@Override
	public Usuario validacionRucUsuario(String ruc) throws Exception {
		return usuarioRepository.validacionRucUsuario(ruc);
	}

	@Override
	@Transactional
	public void enviarCorreoElectronicoRecuperarClave(Usuario usuario) throws Exception {
		
		usuarioRepository.limpiarClaveParaGenerarNuevaClave(usuario.getId());
		
		ParametrosServidorCorreo parametrosServidorCorreo = parametrosServidorCorreoRepository.findById(1).get();
		
		ParametrosGenerales parametrosGenerales = parametrosGeneralesRepository.findById(1).get();
		
		ParametrosGenerales parametrosGeneralesDireccionUrl = parametrosGeneralesRepository.findById(3).get();

		
		String plantillaNotifacion = obtenerNotificacionHtmlDesdeArchivo(parametrosGenerales.getDato2());
		
		String idUsuarioEncriptado = EncriptaUtil.encriptarIdUsuario(String.valueOf(usuario.getId()));

		String urlIdUsuario = parametrosGeneralesDireccionUrl.getDato1() + "login?c=" +  idUsuarioEncriptado;

			
		Map<String, Object> mapaInformacionCorreo = new HashMap<String, Object>();
		mapaInformacionCorreo.put("NOMBRE", usuario.getNombres()+" "+usuario.getApellidos());
		mapaInformacionCorreo.put("CORREOELECTRONICO", usuario.getLogin());	
		mapaInformacionCorreo.put("url",urlIdUsuario);

		
		EmailConfig emailConfig = new EmailConfig();
		emailConfig.setSmtpHost(parametrosServidorCorreo.getHost());
		emailConfig.setSmtpAuth(parametrosServidorCorreo.getAuth());
		emailConfig.setSmtpUser(parametrosServidorCorreo.getUsuario());
		emailConfig.setSmtpPassword(parametrosServidorCorreo.getClave());
		emailConfig.setSmtpStartTlsEnable(parametrosServidorCorreo.getStarttls_enable());
		emailConfig.setSmtpPort(parametrosServidorCorreo.getPort());
		emailConfig.setSmtpSslTrust(parametrosServidorCorreo.getSsl_trust());
		
		List<Email> listaEmail = new ArrayList<>();
		Email email = new Email();
		email.setEmailFrom(parametrosServidorCorreo.getCorreoRemitente());
		email.setEmailTo(usuario.getCorreoElectronico());
		email.setEmailSubject("¿SOLICITASTE CAMBIAR TU CONTRASEÑA?");
		email.setEmailBody(EmailUtil.reemplazarValoresPlantillaHtmlSolicitudCambioCorreo(plantillaNotifacion,mapaInformacionCorreo));
		listaEmail.add(email);
		
		EmailTemplate emailTemplate = new EmailTemplate();
		emailTemplate.setEmailConfig(emailConfig);
		emailTemplate.setListaEmail(listaEmail);
		
		EmailUtil.send(emailTemplate);
	}

	@Override
	public Usuario buscarPorLogin(String login) throws Exception {
		return usuarioRepository.buscarPorLogin(login);
	}
	
	private Usuario mantenerClaveActualSiClaveActualizarEsVacio(Usuario usuario) throws Exception{
		if( usuario.getId()!= null && usuario.getId() > 0) {
			Usuario usuarioActualizar = usuarioRepository.findById(usuario.getId()).get();
			if( usuario.getClave()==null || "".equals(usuario.getClave()) ) {
				usuario.setClave(usuarioActualizar.getClave());
			}
		}
		return usuario;
	}
	/**Llinan**/
}
