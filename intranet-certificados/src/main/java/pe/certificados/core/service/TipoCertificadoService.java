package pe.certificados.core.service;

import java.util.List;

import pe.certificados.core.domain.TipoCertificado;

public interface TipoCertificadoService {
	
	String buscarPorId(Integer id) throws Exception;
	List<TipoCertificado> listarTodos() throws Exception;


}
