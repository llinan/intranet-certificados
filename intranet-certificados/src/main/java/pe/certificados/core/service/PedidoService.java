package pe.certificados.core.service;

import java.util.List;

import pe.certificados.core.bean.ArchivoRespuesta;
import pe.certificados.core.domain.Pedido;
import pe.certificados.core.domain.PedidoDetalle;

public interface PedidoService {
	
	Pedido guardar(Pedido pedido) throws Exception;
	
	Pedido obtenerPedido(Integer id) throws Exception;
	
	Pedido guardarDetallePedido(Pedido pedido) throws Exception;
	
	List<Pedido> listarPedidoPorUsuario(Integer idUsuario) throws Exception;
	
	PedidoDetalle obtenerDetallePedidoPorIdPedido(Integer idPedido) throws Exception;
	
	void actualizarEstadoPedido(Integer id, Integer idEstadoPedido) throws Exception;
	
	void agregarImagen(Integer id, String rutaImagen) throws Exception;
	
	ArchivoRespuesta descargarPdf(Integer id) throws Exception;

	List<Pedido> listarTodos() throws Exception;
	
	Integer obtenerPedidosPorUsuarioEstadoPagoConfirmado(Integer idUsuario) throws Exception;

}
