package pe.certificados.core.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.certificados.core.domain.ParametrosServidorCorreo;
import pe.certificados.core.repository.ParametrosServidorCorreoRepository;
import pe.certificados.core.service.ParametrosServidorCorreoService;


@Service
public class ParametrosServidorCorreoServiceImpl implements ParametrosServidorCorreoService{

	@Autowired
	ParametrosServidorCorreoRepository parametrosServidorCorreoRepository;

	@Override
	public ParametrosServidorCorreo buscarPorId(Integer id) throws Exception {
		return parametrosServidorCorreoRepository.findById(id).get();
	}
	
	
}
