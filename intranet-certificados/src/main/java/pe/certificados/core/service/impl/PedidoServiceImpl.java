package pe.certificados.core.service.impl;


import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import pe.certificados.core.bean.ArchivoRespuesta;
import pe.certificados.core.domain.Pedido;
import pe.certificados.core.domain.PedidoDetalle;
import pe.certificados.core.domain.PedidoNumero;
import pe.certificados.core.repository.PedidoDetalleRepository;
import pe.certificados.core.repository.PedidoNumeroRepository;
import pe.certificados.core.repository.PedidoRepository;
import pe.certificados.core.service.PedidoService;


@Service
public class PedidoServiceImpl implements PedidoService{

	@Autowired
	PedidoRepository pedidoRepository;
	
	@Autowired
	PedidoNumeroRepository pedidoNumeroRepository;
	
	@Autowired
	PedidoDetalleRepository pedidoDetalleRepository;

	@Override
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public Pedido guardar(Pedido pedido) throws Exception {
		
		if(pedido.getId() == null){
			pedido.setNumero(obtenerEIncrementarCorrelativoActual(pedido));
			pedido = pedidoRepository.save(pedido);
		}else{
			pedido = pedidoRepository.save(pedido);
		}
		
		return pedido;
	}
	
	private Long obtenerEIncrementarCorrelativoActual(Pedido pedido) throws Exception {
		
		PedidoNumero pedidoNumero = pedidoNumeroRepository.findById(1).get();
		pedidoNumeroRepository.actualizarCorrelativo(pedidoNumero.getId(), pedidoNumero.getCorrelativo() + 1);
		
		return pedidoNumero.getCorrelativo();
	}


	@Override
	public Pedido obtenerPedido(Integer id) throws Exception {
		return pedidoRepository.findById(id).get();
	}


	@Override
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public Pedido guardarDetallePedido(Pedido pedido) throws Exception {
		
		PedidoDetalle detallePedidoPorIdPedido = pedidoDetalleRepository.obtenerPedidoDetallePorIdPedido(pedido.getId());
		
		pedidoRepository.actualizarEstadoPedido(pedido.getId(), pedido.getIdEstadoPedido());
		
		for(PedidoDetalle pedidoDetalle : pedido.getPedidosDetalle()) {
			pedidoDetalle.setPedido(new Pedido(pedido.getId()));
			if(detallePedidoPorIdPedido == null){
				pedidoDetalleRepository.save(pedidoDetalle);
			}else{
				pedidoDetalle.setId(detallePedidoPorIdPedido.getId());
				pedidoDetalleRepository.save(pedidoDetalle);
			}
		}
		return pedido;
	}

	@Override
	public List<Pedido> listarPedidoPorUsuario(Integer idUsuario) throws Exception {
		List<Pedido> listaPedidos = pedidoRepository.obtenerPedidoPorIdUsuario(idUsuario);		
		return listaPedidos;
	}

	@Override
	public PedidoDetalle obtenerDetallePedidoPorIdPedido(Integer idPedido) throws Exception {
		return pedidoDetalleRepository.obtenerPedidoDetallePorIdPedido(idPedido);
	}

	@Override
	public void actualizarEstadoPedido(Integer id, Integer idEstadoPedido) throws Exception {
		pedidoRepository.actualizarEstadoPedido(id, idEstadoPedido);
	}

	@Override
	public void agregarImagen(Integer id, String rutaImagen) throws Exception {
		Date fechaPago = new Date();
		pedidoRepository.agregarImagenRuta(id, rutaImagen,fechaPago);
	}

	@Override
	public ArchivoRespuesta descargarPdf(Integer id) throws Exception {
		ArchivoRespuesta archivo = new ArchivoRespuesta();

		try {
			Pedido pedido = pedidoRepository.findById(id).get();
			
			Path path = Paths.get(pedido.getRutaImagen());
			
		    archivo.setNombre(path.getFileName().toString());
		    archivo.setByteArrayResource(new ByteArrayResource(Files.readAllBytes(path)));
		    archivo.setTamanio(Files.size(path));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return archivo;
	}
	
	public void convertirImgToPdf() {
//		 Document document = new Document();
//	        String input = "resources/GMARBLES.png"; // .gif and .jpg are ok too!
//	        String output = "resources/GMARBLES.pdf";
//	        try {
//	            FileOutputStream fos = new FileOutputStream(output);
//	            PdfWriter writer = PdfWriter.getInstance(document, fos);
//	            writer.open();
//	            document.open();
//	            document.add(Image.getInstance(input));
//	            document.close();
//	            writer.close();
//	        } catch (Exception e) {
//	            e.printStackTrace();
//	        }
	}

	@Override
	public List<Pedido> listarTodos() throws Exception {
		return pedidoRepository.listarTodos();
	}

	@Override
	public Integer obtenerPedidosPorUsuarioEstadoPagoConfirmado(Integer idUsuario) throws Exception {
		return pedidoRepository.obtenerPedidosPorUsuarioEstadoPagoConfirmado(idUsuario);
	}
	
}
