package pe.certificados.core.service;


import pe.certificados.core.domain.Precio;

public interface PrecioService {
	

	Precio obtenerPrecioPorTipoAliadoYTiempoVigencia(int idTiempoVigencia, int idTipoAliado) throws Exception;

}
