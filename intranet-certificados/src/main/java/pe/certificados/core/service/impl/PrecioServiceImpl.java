package pe.certificados.core.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.certificados.core.domain.Precio;
import pe.certificados.core.repository.PrecioRepository;
import pe.certificados.core.service.PrecioService;


@Service
public class PrecioServiceImpl implements PrecioService{

	@Autowired
	PrecioRepository precioRepository;

	@Override
	public Precio obtenerPrecioPorTipoAliadoYTiempoVigencia(int idTiempoVigencia, int idTipoAliado) throws Exception {
		return precioRepository.obtenerPrecioPorTipoAliadoYTiempoVigencia(idTiempoVigencia, idTipoAliado);
	}
	
	
}
