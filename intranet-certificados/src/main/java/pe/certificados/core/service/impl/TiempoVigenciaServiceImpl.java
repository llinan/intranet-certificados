package pe.certificados.core.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.certificados.core.domain.TiempoVigencia;
import pe.certificados.core.domain.TipoCertificado;
import pe.certificados.core.repository.TiempoVigenciaRepository;
import pe.certificados.core.repository.TipoCertificadoRepository;
import pe.certificados.core.service.TiempoVigenciaService;
import pe.certificados.core.service.TipoCertificadoService;


@Service
public class TiempoVigenciaServiceImpl implements TiempoVigenciaService{

	@Autowired
	TiempoVigenciaRepository tiempoVigenciaRepository;

	@Override
	public List<TiempoVigencia> listarTodos() throws Exception {
		return tiempoVigenciaRepository.findAll();
	}

	@Override
	public String buscarPorId(Integer id) throws Exception {
		TiempoVigencia tiempoVigencia = tiempoVigenciaRepository.findById(id).get();
		return tiempoVigencia.getTiempo();
	}
	
	
}
