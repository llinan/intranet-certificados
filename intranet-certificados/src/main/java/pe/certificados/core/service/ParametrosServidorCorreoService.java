package pe.certificados.core.service;


import pe.certificados.core.domain.ParametrosServidorCorreo;

public interface ParametrosServidorCorreoService {
	
	ParametrosServidorCorreo buscarPorId(Integer id) throws Exception;


}
