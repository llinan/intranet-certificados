package pe.certificados.core.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.certificados.core.domain.TipoCertificado;
import pe.certificados.core.repository.TipoCertificadoRepository;
import pe.certificados.core.service.TipoCertificadoService;


@Service
public class TipoCertificadoServiceImpl implements TipoCertificadoService{

	@Autowired
	TipoCertificadoRepository tipoCertificadoRepository;

	@Override
	public List<TipoCertificado> listarTodos() throws Exception {
		return tipoCertificadoRepository.listarRegistrosActivos();
	}

	@Override
	public String buscarPorId(Integer id) throws Exception {
		TipoCertificado tipoCertificado = tipoCertificadoRepository.findById(id).get();
		return tipoCertificado.getNombre();
	}
	
	
}
