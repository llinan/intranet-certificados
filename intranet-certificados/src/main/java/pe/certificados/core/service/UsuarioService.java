package pe.certificados.core.service;

import java.util.List;

import pe.certificados.core.domain.Usuario;

public interface UsuarioService {
	
	List<Usuario> listarTodos() throws Exception;
	
	Usuario guardar(Usuario usuario) throws Exception;
	
	Usuario buscarPorId(Integer id) throws Exception;
	
	Usuario iniciarSesion(String usuario, String clave) throws Exception;

	Usuario validacionDniUsuario(String login) throws Exception;
	
	void eliminar(Integer id) throws Exception;
	
	void habilitarInhabilitar(Integer id, Integer estadoRegistro) throws Exception;
	
	void enviarNotificacionCorreoElectronico(Usuario usuario) throws Exception;
	
	String obtenerNombreApellidoUsuario(Integer id) throws Exception;
	
	void crearClaveUsuario(Integer id, String clave) throws Exception;
	
	void actualizarNumeroPedidosPagados(Integer id) throws Exception;
	
	Usuario validacionRucUsuario(String ruc) throws Exception;
	
	void enviarCorreoElectronicoRecuperarClave(Usuario usuario) throws Exception;
	
	Usuario buscarPorLogin(String login) throws Exception;
}
