package pe.certificados.core.service;


import java.util.List;

import pe.certificados.core.domain.TipoAliado;

public interface TipoAliadoService {
	
	TipoAliado buscarPorId(Integer id) throws Exception;

	List<TipoAliado> listarTodos() throws Exception;

}
