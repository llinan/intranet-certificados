package pe.certificados.core.type;

import java.util.HashMap;
import java.util.Map;

public enum RolUsuario {
	
	ADMINISTRADOR(1),
	PRINCIPAL(2),
	SECUNDARIO(3);
	
	private static final Map<Integer, RolUsuario> roles = new HashMap<Integer, RolUsuario>();
	
	private final int id;
	
	static {
        for (RolUsuario rol : RolUsuario.values() ) {
        	roles.put(rol.getId() , rol);
        }
    }

	private RolUsuario(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	
	public static RolUsuario obtenerPorId(Integer id) throws Exception {
		return  roles.get(id);
    }
	
}
