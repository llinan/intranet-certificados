package pe.certificados.core.type;

import java.util.HashMap;
import java.util.Map;

public enum TipoDocumentoElectronico {

	TODOS(0, "00", "TODOS"),
	FACTURA_ELECTRONICA(1, "01", "FACTURA ELECTRÓNICA"),
	BOLETA_ELECTRONICA(2, "03", "BOLETA ELECTRÓNICA"),
	NOTA_CREDITO_ELECTRONICA(3, "07", "NOTA DE CRÉDITO ELECTRÓNICA"),
	NOTA_DEBITO_ELECTRONICA(4, "08", "NOTA DE DÉBITO ELECTRÓNICA"),
	GUIA_REMISION_REMITENTE(5, "09", "GUÍA DE REMISIÓN REMITENTE"),
	GUIA_REMISION_TRANSPORTISTA(6, "31", "GUÍA DE REMISION TRANSPORTISTA"),
	ORDEN_COMPRA(7, "-", "ORDEN DE COMPRA");
	
	private static final Map<Integer, TipoDocumentoElectronico> documentosElectronicos = new HashMap<Integer, TipoDocumentoElectronico>();
	
	private final int id;
	
	private final String codigoSunat;
	
	private final String descripcion;
	
	static {
        for (TipoDocumentoElectronico documentoElectronico : TipoDocumentoElectronico.values() ) {
        	documentosElectronicos.put(documentoElectronico.getId(), documentoElectronico);
        }
    }
	
	private TipoDocumentoElectronico(int id, String codigoSunat, String descripcion) {
		this.id = id;
		this.codigoSunat = codigoSunat;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public String getCodigoSunat() {
		return codigoSunat;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public static TipoDocumentoElectronico obtenerPorId(Integer id) {
        return documentosElectronicos.get(id);
    }
	

}
