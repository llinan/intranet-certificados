package pe.certificados.core.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum TipoAliadoEnum {

	BASIC(1,"Basic",1,5),
	BRONCE(2,"Bronce",6,20),
	PLATA(3,"Plata",21,50),
	ORO(4,"Oro",51,80),
	PLATINIUM(5,"Platinium",81,120),
	VIP(6,"VIP",121,500);
	
	private final int id;
	private final String nombre;
	private final int minimo;
	private final int maximo;

    private static final Map<Integer, TipoAliadoEnum> lookup = new HashMap<Integer, TipoAliadoEnum>();

    static {
        for(TipoAliadoEnum e : EnumSet.allOf(TipoAliadoEnum.class))
            lookup.put(e.getId(), e);
    }
    
	private TipoAliadoEnum(int id, String nombre, int minimo, int maximo) {
		this.id = id;
		this.nombre = nombre;
		this.minimo = minimo;
		this.maximo = maximo;
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public int getMinimo() {
		return minimo;
	}

	public int getMaximo() {
		return maximo;
	}
	
	public static TipoAliadoEnum obtener(int codigo) { 
        return lookup.get(codigo); 
    }
	
}
