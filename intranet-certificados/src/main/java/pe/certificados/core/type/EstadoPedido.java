package pe.certificados.core.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum EstadoPedido {	
	TODOS(0, "Todos"), 
	EN_PROCESO(1, "En proceso"), 
	PENDIENTE_PAGAR(2, "Pendiente por pagar"),
	PAGO_CONFIRMAR(3, "Pago por confirmar"),
	VALIDACION_COMPLETA(4, "Validación completa"),
	CERTIFICADO_EMITIDO(5, "Certificado emitido"),
	CERTIFICADO_DESCARGADO(6, "Certificado descargado"),
	FACTURA_EMITIDA(7, "Factura emitida"),
	PAGO_CONFIRMADO(8, "Pago confirmado");

	
	private final int id;
	private final String nombre;
	
    private static final Map<Integer, EstadoPedido> lookup = new HashMap<Integer, EstadoPedido>();

    static {
        for(EstadoPedido e : EnumSet.allOf(EstadoPedido.class))
            lookup.put(e.getId(), e);
    }
	
	private EstadoPedido(int id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	
	public int getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	
    public static EstadoPedido obtener(int codigo) { 
        return lookup.get(codigo); 
    }

}
