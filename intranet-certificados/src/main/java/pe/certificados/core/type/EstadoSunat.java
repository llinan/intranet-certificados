package pe.certificados.core.type;

import java.io.Serializable;

@SuppressWarnings("serial")
public class EstadoSunat implements Serializable{
	
	private String codigo;
	
	private String descripcion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
