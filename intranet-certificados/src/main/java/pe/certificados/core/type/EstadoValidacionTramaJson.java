package pe.certificados.core.type;

public enum EstadoValidacionTramaJson {

	CORRECTA(1, "La trama ha sido procesada correctamente."),
	ERROR(0, "Ha ocurrido un error al procesar la trama");
	
	private final int codigo;
	
	private final String descripcion;
	
	private EstadoValidacionTramaJson(int codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
