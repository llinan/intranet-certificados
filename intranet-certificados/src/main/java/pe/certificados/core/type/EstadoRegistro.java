package pe.certificados.core.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum EstadoRegistro {

	TODOS(0, "Todos"), 
	ACTIVO(1, "Activo"), 
	INACTIVO(2, "Inactivo");
	
	private final int id;
	private final String nombre;
	
    private static final Map<Integer, EstadoRegistro> lookup = new HashMap<Integer, EstadoRegistro>();

    static {
        for(EstadoRegistro e : EnumSet.allOf(EstadoRegistro.class))
            lookup.put(e.getId(), e);
    }
	
	private EstadoRegistro(int id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	
	public int getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	
    public static EstadoRegistro obtener(int codigo) { 
        return lookup.get(codigo); 
    }
	
}
