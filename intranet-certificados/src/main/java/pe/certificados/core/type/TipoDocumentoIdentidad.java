package pe.certificados.core.type;

import java.util.HashMap;
import java.util.Map;

public enum TipoDocumentoIdentidad {

	DOC_TRIB_NO_DOM_SIN(1, "DOC. TRIB. NO. DOM. SIN RUC", "0"),
	DNI(2, "DNI", "1"),
	CARNET_EXTRANJERIA(3, "CARNET DE EXTRANJERIA", "4"),
	RUC(4, "RUC", "6"),
	PASAPORTE(5, "PASAPORTE", "7"),
	CED_DIPLOMATICA_IDENTIDAD(6, "CED. DIPLOMATIVA DE IDENTIDAD", "A");
	
	private static final Map<String, TipoDocumentoIdentidad> documentosIdentidad = new HashMap<String, TipoDocumentoIdentidad>();
	
	private final int id;
	
	private final String abreviatura;
	
	private final String codigoSunat;
	
	static {
        for (TipoDocumentoIdentidad documentoIdentidad : TipoDocumentoIdentidad.values() ) {
        	documentosIdentidad.put(documentoIdentidad.getCodigoSunat(), documentoIdentidad);
        }
    }
	
	private TipoDocumentoIdentidad(int id, String abreviatura, String codigoSunat) {
		this.id = id;
		this.abreviatura = abreviatura;
		this.codigoSunat = codigoSunat;
	}

	public int getId() {
		return id;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public String getCodigoSunat() {
		return codigoSunat;
	}
	
	public static TipoDocumentoIdentidad obtenerPorCodigoSunat(String codigoSunat) throws Exception {
		TipoDocumentoIdentidad documentoIdentidad = documentosIdentidad.get(codigoSunat);
		if( documentoIdentidad == null ) {
			throw new NullPointerException("No se ha podido obtener el documento de identidad para el codigo: " + codigoSunat);
		}
        return documentoIdentidad;
    }
	
}
