package pe.certificados.core.bean;

import org.springframework.core.io.ByteArrayResource;

public class ArchivoRespuesta {

	private String nombre;
	
	private ByteArrayResource byteArrayResource;
	
	private long tamanio;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ByteArrayResource getByteArrayResource() {
		return byteArrayResource;
	}

	public void setByteArrayResource(ByteArrayResource byteArrayResource) {
		this.byteArrayResource = byteArrayResource;
	}

	public long getTamanio() {
		return tamanio;
	}

	public void setTamanio(long tamanio) {
		this.tamanio = tamanio;
	}
	
}
