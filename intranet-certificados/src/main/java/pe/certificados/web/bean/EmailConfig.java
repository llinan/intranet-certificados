package pe.certificados.web.bean;

public class EmailConfig {
	
	private String smtpHost;
	private int smtpPort;
	private String smtpAuth;
	private String smtpUser;
	private String smtpPassword;
	private String smtpStartTlsEnable;
	private String smtpSslTrust;
	private String smtpSocketFactoryPort;
	private String smtpSocketFactoryClass;
	private String smtpAuthMechanisms;
	private String smtpAuthNtlmDomain;
	
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public int getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getSmtpAuth() {
		return smtpAuth;
	}
	public void setSmtpAuth(String smtpAuth) {
		this.smtpAuth = smtpAuth;
	}
	public String getSmtpUser() {
		return smtpUser;
	}
	public void setSmtpUser(String smtpUser) {
		this.smtpUser = smtpUser;
	}
	public String getSmtpPassword() {
		return smtpPassword;
	}
	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}
	public String getSmtpStartTlsEnable() {
		return smtpStartTlsEnable;
	}
	public void setSmtpStartTlsEnable(String smtpStartTlsEnable) {
		this.smtpStartTlsEnable = smtpStartTlsEnable;
	}
	public String getSmtpSslTrust() {
		return smtpSslTrust;
	}
	public void setSmtpSslTrust(String smtpSslTrust) {
		this.smtpSslTrust = smtpSslTrust;
	}
	public String getSmtpSocketFactoryPort() {
		return smtpSocketFactoryPort;
	}
	public void setSmtpSocketFactoryPort(String smtpSocketFactoryPort) {
		this.smtpSocketFactoryPort = smtpSocketFactoryPort;
	}
	public String getSmtpSocketFactoryClass() {
		return smtpSocketFactoryClass;
	}
	public void setSmtpSocketFactoryClass(String smtpSocketFactoryClass) {
		this.smtpSocketFactoryClass = smtpSocketFactoryClass;
	}
	public String getSmtpAuthMechanisms() {
		return smtpAuthMechanisms;
	}
	public void setSmtpAuthMechanisms(String smtpAuthMechanisms) {
		this.smtpAuthMechanisms = smtpAuthMechanisms;
	}
	public String getSmtpAuthNtlmDomain() {
		return smtpAuthNtlmDomain;
	}
	public void setSmtpAuthNtlmDomain(String smtpAuthNtlmDomain) {
		this.smtpAuthNtlmDomain = smtpAuthNtlmDomain;
	}
	
	
}