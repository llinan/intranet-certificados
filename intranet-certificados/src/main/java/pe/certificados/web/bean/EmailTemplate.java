package pe.certificados.web.bean;

import java.util.List;

public class EmailTemplate {
	
	List<Email> listaEmail;
	EmailConfig emailConfig;
	
	public List<Email> getListaEmail() {
		return listaEmail;
	}
	public void setListaEmail(List<Email> listaEmail) {
		this.listaEmail = listaEmail;
	}
	public EmailConfig getEmailConfig() {
		return emailConfig;
	}
	public void setEmailConfig(EmailConfig emailConfig) {
		this.emailConfig = emailConfig;
	}
	
	
	
}