package pe.certificados.web.bean;

public class Email {
	
	private String emailFrom; //de:
	private String emailFromAlias; // de - alias:
	private String emailTo; // para
	private String emailSubject; //asunto
	private String emailCC; //copia
	private String emailBCC; //oculto
	private String emailReplyTo; // responder a
	private String emailBody; //cuerpo
	private String emailSigned; //firmado
	
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public String getEmailFromAlias() {
		return emailFromAlias;
	}
	public void setEmailFromAlias(String emailFromAlias) {
		this.emailFromAlias = emailFromAlias;
	}
	public String getEmailTo() {
		return emailTo;
	}
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	public String getEmailCC() {
		return emailCC;
	}
	public void setEmailCC(String emailCC) {
		this.emailCC = emailCC;
	}
	public String getEmailBCC() {
		return emailBCC;
	}
	public void setEmailBCC(String emailBCC) {
		this.emailBCC = emailBCC;
	}
	public String getEmailReplyTo() {
		return emailReplyTo;
	}
	public void setEmailReplyTo(String emailReplyTo) {
		this.emailReplyTo = emailReplyTo;
	}
	public String getEmailBody() {
		return emailBody;
	}
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	public String getEmailSigned() {
		return emailSigned;
	}
	public void setEmailSigned(String emailSigned) {
		this.emailSigned = emailSigned;
	}
	
	
	
}