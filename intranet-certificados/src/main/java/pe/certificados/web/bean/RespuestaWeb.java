package pe.certificados.web.bean;

import static java.text.MessageFormat.format;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import pe.certificados.web.type.TipoRespuestaWeb;

@SuppressWarnings("serial")
public class RespuestaWeb implements Serializable {
	
	private int codigoRespuesta;
    
	private String mensajeRespuesta;
    
	private Map<String, Object> parametros;
    
    public RespuestaWeb() {
		parametros = new HashMap<String, Object>();
	}
    
//    public RespuestaWeb() {
//    	codigoRespuesta = tipoRespuestaWeb.getCodigo();
//    	mensajeRespuesta = format(tipoRespuestaWeb.getMensaje(), idError);
//		parametros = new HashMap<String, Object>();
//	}
    
    public int getCodigoRespuesta() {
        return codigoRespuesta;
    }
    public void setCodigoRespuesta(int codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }
    public String getMensajeRespuesta() {
        return mensajeRespuesta;
    }
    public void setMensajeRespuesta(String mensajeRespuesta) {
        this.mensajeRespuesta = mensajeRespuesta;
    }
    public Map<String, Object> getParametros() {
    	if(parametros==null){
    		parametros = new HashMap<String, Object>();
    	}
        return parametros;
    }
    
    public void setTipoRespuesta(TipoRespuestaWeb tipoRespuestaWeb) {
    	this.codigoRespuesta = tipoRespuestaWeb.getCodigo();
    	this.mensajeRespuesta = tipoRespuestaWeb.getMensaje();
    }
    
    public void setTipoRespuesta(TipoRespuestaWeb tipoRespuestaWeb, String idError) {
    	this.codigoRespuesta = tipoRespuestaWeb.getCodigo();
    	this.mensajeRespuesta = format(tipoRespuestaWeb.getMensaje(), idError);
    }
    
    //TODO: eliminar!!
//    public static RespuestaWeb obtenerRespuestaWeb(TipoRespuestaWeb tipoRespuesta) {
//    	RespuestaWeb respuestaWeb = new RespuestaWeb();
//    	respuestaWeb.setCodigoRespuesta(tipoRespuesta.getCodigo());
//    	respuestaWeb.setMensajeRespuesta(tipoRespuesta.getMensaje());
//    	return respuestaWeb;
//    	
//    }

	@Override
	public String toString() {
		return "RespuestaWeb [codigoRespuesta=" + codigoRespuesta + ", mensajeRespuesta=" + mensajeRespuesta
				+ ", parametros=" + parametros + "]";
	}
    
    
}
