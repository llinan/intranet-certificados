package pe.certificados.web.session.bean;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.certificados.core.domain.Opciones;
import pe.certificados.core.domain.Usuario;
import pe.certificados.core.type.RolUsuario;

public class UsuarioAutenticado implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private static final int NIVEL_MENU = 1;
	
	private static final String CERRAR_SESION_URL = "/login/cerrarSesion";

	private int id;
	
//	private int idTipoUsuario;
	
	private String login;
	
	private String nombres;
		
	private String apellidos;
	
	private int idTipoAliado;
		
	private String opcionesHtml;
	
	private List<String> codigoOpciones;
	
	private String tipo;
	
	public UsuarioAutenticado(Usuario usuario) {
		this.id = usuario.getId();
		this.login = usuario.getLogin();
		this.nombres = usuario.getNombres();
		this.apellidos = usuario.getApellidos();
		this.idTipoAliado = usuario.getTipoAliado().getId();
		this.opcionesHtml = getOpcionesHtml(usuario);
		this.codigoOpciones = getCodigoOpciones(usuario.getOpciones());
		this.tipo = usuario.getTipo();
	}
	
	private List<String> getCodigoOpciones(List<Opciones> opciones){
		List<String> codigoOpciones = new ArrayList<String>();
		opciones.forEach(opcion -> {
			codigoOpciones.add(opcion.getCodigoOpcion());
		});
		return codigoOpciones;
	}
	
	private String getOpcionesHtml(Usuario usuario){
		
		StringBuffer sbOpcionesHtml = new StringBuffer();
		
		List<Opciones> opciones = usuario.getOpciones();
		opciones.stream().filter( menu -> NIVEL_MENU == menu.getNivel() ).forEach(menu -> {
			
//			sbOpcionesHtml.append("<ul class=\"nav navbar-nav\">");
//			sbOpcionesHtml.append("<li class=\"dropdown\">");
//			sbOpcionesHtml.append("<a href=\"\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">");
//			sbOpcionesHtml.append("<i class=\"fa fa-angle-down m-r-1\"></i>");
//			sbOpcionesHtml.append(menu.getNombre());
//			sbOpcionesHtml.append("</a>");
			
//			sbOpcionesHtml.append("<ul class=\"dropdown-menu\">");
//			opciones.stream().filter( submenu -> menu.getCodigoOpcion().equals(submenu.getCodigoOpcionPadre())).forEach( submenu -> {
				sbOpcionesHtml.append("<li>");
				sbOpcionesHtml.append("<span class=\"");
				sbOpcionesHtml.append("icon");
				sbOpcionesHtml.append("\">");
				sbOpcionesHtml.append("<i class=\"");
				sbOpcionesHtml.append(menu.getIconoClass());
				sbOpcionesHtml.append("\"></span></i>");
				sbOpcionesHtml.append("<a href=\"");
				sbOpcionesHtml.append(menu.getAccion());
				sbOpcionesHtml.append("\">");
				sbOpcionesHtml.append(menu.getNombre());
				sbOpcionesHtml.append("</a></li>");
//			});
//			sbOpcionesHtml.append("</ul></li></ul>");
		});
		
//		sbOpcionesHtml.append("<ul class=\"nav navbar-nav navbar-right\">");
//		sbOpcionesHtml.append("<li class=\"dropdown\">");
//		sbOpcionesHtml.append("<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">");
//		sbOpcionesHtml.append("<i class=\"fa fa-gear m-r-1\"></i>");
//		sbOpcionesHtml.append(this.getNombres() + " " + this.getApellidos());
//		sbOpcionesHtml.append("</a>");
//		sbOpcionesHtml.append("<ul class=\"dropdown-menu\">");
//		sbOpcionesHtml.append("<li><a href=\"\">Cambiar contraseña</a></li>");
//		sbOpcionesHtml.append("<li class=\"divider\"></li>");
//    	sbOpcionesHtml.append("<li><a href=\"" + CERRAR_SESION_URL + "\">Cerrar sesión</a></li>");
//    	sbOpcionesHtml.append("</ul></li></ul>");

		return sbOpcionesHtml.toString();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

//	public int getIdTipoUsuario() {
//		return idTipoUsuario;
//	}
//
//	public void setIdTipoUsuario(int idTipoUsuario) {
//		this.idTipoUsuario = idTipoUsuario;
//	}
	
	public String getOpcionesHtml() {
		return opcionesHtml;
	}
	
	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public List<String> getCodigoOpciones() {
		return codigoOpciones;
	}

	public int getIdTipoAliado() {
		return idTipoAliado;
	}

	public void setIdTipoAliado(int idTipoAliado) {
		this.idTipoAliado = idTipoAliado;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "UsuarioAutenticado [id=" + id + ", login=" + login + ", nombres=" + nombres + ", apellidos=" + apellidos
				+ ", idTipoAliado=" + idTipoAliado + ", opcionesHtml=" + opcionesHtml + ", codigoOpciones="
				+ codigoOpciones + "]";
	}

//	@Override
//	public String toString() {
//		return "UsuarioAutenticado [id=" + id + ", idTipoUsuario=" + idTipoUsuario + ", login=" + login + ", nombres="
//				+ nombres + ", apellidoMaterno=" + apellidoMaterno + ", apellidoPaterno=" + apellidoPaterno
//				+ ", idEmpresa=" + idEmpresa + ", idLocal=" + idLocal + ", codigoEstablecimiento="
//				+ codigoEstablecimiento + ", rutaRepositorio=" + rutaRepositorio + ", versionUblFactura="
//				+ versionUblFactura + ", versionDocumentoFactura=" + versionDocumentoFactura
//				+ ", certificadoDigitalRuta=" + certificadoDigitalRuta + ", certificadoDigitalClave="
//				+ certificadoDigitalClave + "]";
//	}
	
	

}
