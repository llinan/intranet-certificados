package pe.certificados.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import pe.certificados.core.domain.Usuario;
import pe.certificados.web.session.bean.UsuarioAutenticado;
import pe.certificados.web.util.ExceptionUtil;
import pe.certificados.web.util.WebUtil;

@Component
@Order(1)
public class LoginFilter implements Filter {
	
	private static final String[] URL_PERMITIDAS_SIN_AUTENTICACION = {
			"/", 
			"/login",
			"/autenticarUsuario",
			"/login/generarClave",
			"/usuario/guardarClave",
			"/usuario/recuperarClave"
	};
	
	private static final String[] RECURSOS_PERMITIDOS = {
			".css",
			".js",
			".png",
			".jpg",
			".svg",
			"woff",
			"woff2",
			".ico"
	};
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest request   = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        
        String url = request.getRequestURI();
        
        try {
        	
        	if( urlNoEstaPermitidaSinAutenticacion(url) ) {
            	if (isSesionInvalida(request)) {
                    if(isPeticionAjax(request)){
                    	enviarRespuestaSesionExpiradaAjax(request, response);
                        return;
                    }else {
                    	enviarRespuestaSesionExpirada(request, response);
                        return;
                    }
                }
            	
            	UsuarioAutenticado usuarioAutenticado = (UsuarioAutenticado) request.getSession().getAttribute(WebUtil.SESION_USUARIO_AUTENTICADO);
	                if( usuarioAutenticado==null ) {
                	if(isPeticionAjax(request)){
                		enviarRespuestaSesionExpiradaAjax(request, response);
                        return;
                	}else{
                		enviarRespuestaSesionExpirada(request, response);
                		return;
                	}
                }
            }
            	
		} catch (Exception excepcion) {
			ExceptionUtil.controlar(excepcion);
		}
        
		chain.doFilter(request, response);
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	private boolean urlNoEstaPermitidaSinAutenticacion(String url) {
		for(String paginaPermitidasSinAutenticacion : URL_PERMITIDAS_SIN_AUTENTICACION) {
			if ( url.endsWith(paginaPermitidasSinAutenticacion)) {
				return false;
			}
		}
		
		for(String recursoPermitido : RECURSOS_PERMITIDOS) {
			if ( url.contains(recursoPermitido) ) {
				return false;
			}
		}
		
		return true;
	}
	
    private boolean isSesionInvalida(HttpServletRequest request) {
        return  (request.getRequestedSessionId() != null) && !request.isRequestedSessionIdValid();
    }
    
	private boolean isPeticionAjax(HttpServletRequest request) {
		if(request.getHeader("accept")==null) {
			return false;
		}
        return request.getHeader("accept").contains("application/json");
	}
	
	//TODO: Revisar el mensaje 'su sesión ha expirado'
    private void enviarRespuestaSesionExpirada(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	response.sendRedirect(request.getContextPath() + "/login" + "?mensaje=Su sesión ha expirado.");
    }
    
    private void enviarRespuestaSesionExpiradaAjax(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String urlLogin = request.getContextPath() + "/login" + "?mensaje=Su sesión ha expirado.";
        response.setContentType("text/plain");
        response.getWriter().write("sesion expirada" + "," + urlLogin);
    }

}
