package pe.certificados.web.util;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.text.SimpleDateFormat;

public class MiFacturaUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(MiFacturaUtil.class);
	
	private static final String FORMATO_FECHA = "yyyy-MM-dd";
	
	public static String formatearNumeroDocumento(long numero) throws Exception{
		try {
			return String.format("%08d", numero);
		} catch (Exception excepcion) {
			LOGGER.error("[MiFacturaUtil] - Ha ocurrido un error al usar el método formatearNumeroDocumento.\n"   
				    + "numero = " + numero ,
                    excepcion);
			throw new Exception("Ha ocurrido un error al usar el método formatearNumeroDocumento.\n numero: " + numero, excepcion);
		}
	}
	
	public static String formaterFecha(Date fecha){
		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat(FORMATO_FECHA);
			return formatoFecha.format(fecha);
		} catch (Exception excepcion) {
			LOGGER.error("[MiFacturaUtil] - Ha ocurrido un error al usar el método formaterFecha.\n"   
				    + "fecha = " + fecha ,
                    excepcion);
		}
		return "";
	}
	
//	public static String obtenerNombreCarpetas() throws Exception{
//		
//		StringBuffer nombreCarpetasJson = new StringBuffer();
//		nombreCarpetasJson.append(FechaUtil.obtenerAnioActual());
//		nombreCarpetasJson.append(File.separator);
//		nombreCarpetasJson.append(FechaUtil.obtenerMesActual());
//		nombreCarpetasJson.append(File.separator);
//		nombreCarpetasJson.append(FechaUtil.obtenerDiaActual());
//		
//		return nombreCarpetasJson.toString();
//	}
	
}
