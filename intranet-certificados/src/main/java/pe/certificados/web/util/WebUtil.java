package pe.certificados.web.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import pe.certificados.core.domain.Usuario;
import pe.certificados.web.session.bean.UsuarioAutenticado;

public class WebUtil {
	
	public static final String SESION_USUARIO_AUTENTICADO = "usuarioAutenticado";
	
	public static UsuarioAutenticado setSesionUsuarioAutenticado(Usuario usuario) throws Exception{
		UsuarioAutenticado usuarioAutenticado = new UsuarioAutenticado(usuario);
		getRequest().getSession().setAttribute(SESION_USUARIO_AUTENTICADO, usuarioAutenticado);
		return usuarioAutenticado;
	}
	
	public static UsuarioAutenticado getUsuarioAutenticado() throws Exception{
		try {
			return (UsuarioAutenticado)getRequest().getSession().getAttribute(SESION_USUARIO_AUTENTICADO);
		} catch (Exception excepcion) {
			throw new Exception("Ha ocurrido un error en el método getUsuarioAutenticado()", excepcion);
		}
	}
	
	public static HttpServletRequest getRequest() throws Exception{
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
	    HttpServletRequest request = servletRequestAttributes.getRequest();
	    return request;
	}
	
//	private static UsuarioAutenticado obtenerDatosEstaticamente() throws Exception{
//		UsuarioAutenticado usuarioAutenticado = new UsuarioAutenticado();
//		usuarioAutenticado.setId(2);
//		usuarioAutenticado.setLogin("CALTAMIRANOB");
//		usuarioAutenticado.setNombres("CARLOS");
//		usuarioAutenticado.setApellidoPaterno("ALTAMIRANO");
//		usuarioAutenticado.setApellidoMaterno("BRICEÑO");
//		usuarioAutenticado.setIdEmpresa(1);
////		usuarioAutenticado.setRucEmpresa("20488429419");
//		usuarioAutenticado.setIdLocal(1);
//		usuarioAutenticado.setCodigoEstablecimiento("0000");
//		usuarioAutenticado.setIdTipoUsuario(RolUsuario.PRINCIPAL.getId());
//		
//		usuarioAutenticado.setRutaRepositorio("C:\\miFactura\\repositorioElectronico\\empresa01\\documentos");
//		usuarioAutenticado.setCertificadoDigitalRuta("C:\\miFactura\\repositorioElectronico\\empresa01\\configuracion\\certificado\\ALRESA.pfx");
//		
////		usuarioAutenticado.setRutaRepositorio("/opt/mifactura/repositorio/empresa01/documentos");
////		usuarioAutenticado.setCertificadoDigitalRuta("/opt/mifactura/repositorio/empresa01/configuracion/certificado/ALRESA.pfx");
//		
//		usuarioAutenticado.setCertificadoDigitalClave("ALRESA");
//		usuarioAutenticado.setVersionUblFactura("2.1");
//		usuarioAutenticado.setVersionDocumentoFactura("2.0");
//		
//		return usuarioAutenticado;
//	}
	
	
	
	
}
