package pe.certificados.web.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FechaUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(FechaUtil.class);
	
	public static String convertirACadena(Date fecha, String formato){
		String fechaFormateada = "";
		try {
			fechaFormateada = new SimpleDateFormat(formato).format(fecha);
		}catch(Exception excepcion) {
			LOGGER.error("[FechaUtil - convertirACadena()] - Ha ocurrido un error al convertir la fecha: " + fecha + " con el formato: " + formato, excepcion);
		}
		return fechaFormateada;
	}
	
	public static int obtenerAnioActual() throws Exception{
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.YEAR);
	}
	
	public static int obtenerMesActual() throws Exception{
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.MONTH);
	}
	
	public static int obtenerDiaActual() throws Exception{
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
}
