package pe.certificados.web.util;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import pe.certificados.web.bean.Email;
import pe.certificados.web.bean.EmailTemplate;

public class EmailUtil {

	private static Logger logger = LoggerFactory.getLogger(EmailUtil.class);
	
	public static void send(EmailTemplate emailTemplate) throws UnsupportedEncodingException, MessagingException{
		long startTime = System.currentTimeMillis();
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		
		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpHost()).ifPresent(mailSender::setHost);
		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpPort()).ifPresent(mailSender::setPort);
		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpUser()).ifPresent(mailSender::setUsername);
		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpPassword()).ifPresent(mailSender::setPassword);
		
		Properties props = mailSender.getJavaMailProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.allow8bitmime", "true");
		props.put("mail.smtps.allow8bitmime", "true");

		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpAuth()).ifPresent(e -> {props.put("mail.smtp.auth", Boolean.parseBoolean(emailTemplate.getEmailConfig().getSmtpAuth()));});
		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpStartTlsEnable()).ifPresent(e -> {props.put("mail.smtp.starttls.enable", Boolean.parseBoolean(emailTemplate.getEmailConfig().getSmtpStartTlsEnable()));});
		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpSslTrust()).ifPresent(e -> {props.put("mail.smtp.ssl.trust", emailTemplate.getEmailConfig().getSmtpSslTrust());});
		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpSocketFactoryPort()).ifPresent(e -> {props.put("mail.smtp.socketFactory.port", emailTemplate.getEmailConfig().getSmtpSocketFactoryPort());});
		Optional.ofNullable(emailTemplate.getEmailConfig().getSmtpSocketFactoryClass()).ifPresent(e -> {props.put("mail.smtp.socketFactory.class", emailTemplate.getEmailConfig().getSmtpSocketFactoryClass());});

		mailSender.setJavaMailProperties(props);

		logger.info("[EmailUtil] sendDefault() {} emails received, prepairing to send...", emailTemplate.getListaEmail().size());

		MimeMessage[] lista = emailTemplate.getListaEmail().stream().map(wrapper(email -> setProperties(mailSender,email))).toArray(MimeMessage[]::new);

		mailSender.send(lista);
		
		long endTime = System.currentTimeMillis();
		
		logger.info("[EmailUtil] sendDefault() {} emails sent in {} ms.", emailTemplate.getListaEmail().size(), (endTime-startTime));
	}
	
	
	private static MimeMessage setProperties(JavaMailSenderImpl mailSender, Email email) throws UnsupportedEncodingException, MessagingException{
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message,true);
		helper.setFrom(email.getEmailFrom(), StringUtils.defaultIfEmpty(email.getEmailFromAlias(), email.getEmailFrom()));
		helper.setTo(email.getEmailTo());
		helper.setText(email.getEmailBody(),true);
		helper.setSubject(email.getEmailSubject());
			
		if (email.getEmailCC() != null) 		helper.setCc(email.getEmailCC());
		if (email.getEmailBCC() != null) 		helper.setBcc(email.getEmailBCC());
		if (email.getEmailReplyTo() != null)	helper.setReplyTo(email.getEmailReplyTo());
		
		return message;
	}
	
	@FunctionalInterface
	protected interface FunctionWithException<T, R, E extends Exception> {
		R apply(T t) throws E;
	}

	private static <T, R, E extends Exception> Function<T, R> wrapper(FunctionWithException<T, R, E> fe) {
		return arg -> {
			try {
				return fe.apply(arg);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}
	
	public static String reemplazarValoresPlantillaHtmlSolicitudCambioCorreo(String plantillaHtml, Map<String, Object> informacionCorreo) {
		String plantillaHtmlReemplazada = null;
        
		plantillaHtmlReemplazada = plantillaHtml
								  .replace("|CORREOELECTRONICO|",informacionCorreo.get("CORREOELECTRONICO").toString())
								  .replace("|NOMBRE|", informacionCorreo.get("NOMBRE").toString())
								  .replace("|URL|", informacionCorreo.get("url").toString());
		
		return plantillaHtmlReemplazada;
	}
}
