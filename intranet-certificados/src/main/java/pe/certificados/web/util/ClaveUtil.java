package pe.certificados.web.util;

import org.apache.commons.codec.digest.DigestUtils;

public class ClaveUtil {

	public static String encriptarClave(String clave) throws Exception{
		try {
			if(clave==null || "".equals(clave)) {
				return "";
			}
			return DigestUtils.sha256Hex(clave);
		} catch (Exception e) {
			throw new Exception("Ha ocurrido un error en el método encriptarClave. Datos: clave = " + clave);
		}
	}
	
}
