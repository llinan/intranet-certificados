package pe.certificados.web.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pe.certificados.web.util.serializer.DateSerializer;

public class JsonUtil {

	public static <T> T obtenerObjetoJsonDeFichero(String ruta, Class<T> clase) throws Exception{
		T objeto = null;
		Gson gson = null;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(ruta));
			GsonBuilder gsonBuilder = new GsonBuilder();
			gson = gsonBuilder.create();
			objeto = gson.fromJson(reader, clase);
		} catch (Exception excepcion) {
			throw new Exception(excepcion);
		} finally{
			reader.close();
		}
		return objeto;
	}
	
	public static Object escribirObjetoEnArchivoJson(Object objeto, String ruta) throws Exception{
		Gson gson = null;
		Writer writer = null;
		try {
			File fileRuta = new File(ruta);
			fileRuta.getParentFile().mkdirs();
			
			writer = new FileWriter(ruta);
			GsonBuilder gsonBuilder = new GsonBuilder();
			gson = gsonBuilder.create();
		    gson.toJson(objeto, writer);
		    return objeto;
		} catch (Exception excepcion) {
			throw new Exception(excepcion);
		} finally {
			writer.close();
		}
	}
	
    public static String convertirObjetoACadenaJson(Object objeto){
        Gson objetoGson = new GsonBuilder().registerTypeAdapter(Date.class, new DateSerializer("dd/MM/yyyy")).create();
        return objetoGson.toJson(objeto);
    }
    
    public static <T> T convertirCadenaJsonAObjeto(String cadena, Class<T> clase){
    	Gson objetoGson=null;
    	try {
    		cadena = URLDecoder.decode(cadena, "UTF-8");
    		GsonBuilder objetoGsonBuilder = new GsonBuilder();
    		objetoGsonBuilder.registerTypeAdapter(Date.class, new DateSerializer("dd/MM/yyyy"));
            objetoGson = objetoGsonBuilder.create();
            objetoGson.fromJson(cadena, clase);
    	} catch (UnsupportedEncodingException e) {
    		e.printStackTrace();
    	}
        return objetoGson.fromJson(cadena, clase);
    }
    
    public static <T> T convertirCadenaJsonAObjetoRequest(String cadena, Class<T> clase){
    	Gson objetoGson=null;
        GsonBuilder objetoGsonBuilder = new GsonBuilder();
        objetoGsonBuilder.registerTypeAdapter(Date.class, new DateSerializer("dd/MM/yyyy"));
        objetoGson = objetoGsonBuilder.create();
        objetoGson.fromJson(cadena, clase);
        return objetoGson.fromJson(cadena, clase);
    }
    
    public static String convertirObjetoACadenaJson(Object objeto, String formatoFecha){
        Gson objetoGson = new GsonBuilder().registerTypeAdapter(Date.class, new DateSerializer(formatoFecha)).create();
        return objetoGson.toJson(objeto);
    }
    
}
