package pe.certificados.web.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Base64;

public class EncriptaUtil {
	
	public static String encriptarParametroUrl(String cadenaSinEncriptar) throws UnsupportedEncodingException {
		String encriptado = null;
		Base32 base64 = new Base32();
		byte[] bytesCadenaSinEncriptar = cadenaSinEncriptar.getBytes(StandardCharsets.UTF_8);
		encriptado = new String(base64.encode(bytesCadenaSinEncriptar), StandardCharsets.UTF_8);
		encriptado = URLEncoder.encode(encriptado, "UTF-8");
		return encriptado;
	}

	public static String desencriptarParametroUrl(String cadenaEncriptada) throws UnsupportedEncodingException {
		String desencriptado = null;
		Base32 base64 = new Base32();
		cadenaEncriptada = URLDecoder.decode(cadenaEncriptada, "UTF-8");
		byte[] bytesCadenaEncriptada = cadenaEncriptada.getBytes(StandardCharsets.UTF_8);
		desencriptado = new String(base64.decode(bytesCadenaEncriptada), StandardCharsets.UTF_8);

		return desencriptado;
	}
	
	public static String encriptarIdUsuario(String idUsuario) throws Exception{
		String token = new BigInteger(130, new SecureRandom()).toString(32);
		String cadenaEncriptada = EncriptaUtil.encriptarParametroUrl(token + "-" + idUsuario);
		return cadenaEncriptada;
	}
	
	public static String desencriptarIdUsuario(String idUsuarioEncriptado) throws Exception{
		String cadenaDesencriptada = EncriptaUtil.desencriptarParametroUrl(idUsuarioEncriptado);
		String idDetalleEnvio = cadenaDesencriptada.split("-")[1];
		return idDetalleEnvio;
	}
	
	public static String encriptarId(String idUsuario) throws Exception{
		String token = new BigInteger(130, new SecureRandom()).toString(32);
		String cadenaEncriptada = EncriptaUtil.encriptarParametroUrl(token + "-" + idUsuario);
		return cadenaEncriptada;
	}
	
	public static String desencriptarId(String idUsuarioEncriptado) throws Exception{
		String cadenaDesencriptada = EncriptaUtil.desencriptarParametroUrl(idUsuarioEncriptado);
		String idDetalleEnvio = cadenaDesencriptada.split("-")[1];
		return idDetalleEnvio;
	}
//	
//	public static String encriptarParametroRecuperarContrasena(String idRecuperarContrasena, String idColaborador) throws Exception{
//		String token = new BigInteger(130, new SecureRandom()).toString(32);
//		String cadenaEncriptada = EncriptaUtil.encriptarParametroUrl(token + "-" + idRecuperarContrasena + "-" + idColaborador);
//		return cadenaEncriptada;
//	}
//	
//	public static String[] desencriptarParametroRecuperarContrasena(String parametroEncriptado) throws Exception{
//		String[] parametroRecuperarContrasena = new String[2];
//		String cadenaDesencriptada = EncriptaUtil.desencriptarParametroUrl(parametroEncriptado);
//		parametroRecuperarContrasena[0] = cadenaDesencriptada.split("-")[1];
//		parametroRecuperarContrasena[1] = cadenaDesencriptada.split("-")[2];
//		return parametroRecuperarContrasena;
//	}
	
	public static String encriptarContrasena(String cadena) { 
        return new String(Base64.encodeBase64(cadena.getBytes()));
    }

    public static String desencriptarContrasena(String cadena) { 
    	return new String(Base64.decodeBase64(cadena.getBytes()));
    } 
}
