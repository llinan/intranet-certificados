package pe.certificados.web.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CadenaUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(CadenaUtil.class);
	
	public static boolean estaVaciaCadena(String cadena){
		try {
			return cadena==null || "".equals(cadena);
		}catch(Exception excepcion) {
			LOGGER.error("[CadenaUtil - estaVaciaCadena()] - Ha ocurrido un error al validar la cadena: " + cadena, excepcion);
		}
		return false;
	}
	
	public static String limpiarCadena(String cadena) {
		try {
			if(cadena==null) {
				return "";
			}
			return cadena.toString();
		} catch (Exception excepcion) {
			LOGGER.error("[CadenaUtil - limpiarCadena()] - Ha ocurrido un error al limpiar la cadena: " + cadena, excepcion);
		}
		return "";
	}
	
	public static String removerUltimoCaracter(String cadena) {
		if( cadena == null || "".equals(cadena.trim()) || cadena.length()==0) {
			return "";
		}
		return cadena.substring(0, cadena.length() - 1 );
	}
	
	
}
