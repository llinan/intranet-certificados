package pe.certificados.web.util;

import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

public class RequestUtil {
	
	public static int parseInt(String valor) {
		return valor == null ? 0 : Integer.parseInt(valor);
	}
	
	public static long parseLong(String valor) {
		return valor == null ? 0 : Long.parseLong(valor);
	}
	
	public static String parseString(String valor) {
		return valor == null ? "" : valor.toUpperCase().trim();
	}
	
	public static Date parseDate(String valor, String formatoFecha) throws Exception{
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatoFecha);
			return simpleDateFormat.parse(valor);
		} catch (Exception e) {
			throw new Exception("Ha ocurrido un error al parsear la fecha.\nDatos: valor = " + valor + ", formatoFecha: " + formatoFecha);
		}
	}
	
	public static double parseDouble(String valor) {
		return valor == null ? 0 : Double.parseDouble(valor);
	}
	
	
}
