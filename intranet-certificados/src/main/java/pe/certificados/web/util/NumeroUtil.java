package pe.certificados.web.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumeroUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(NumeroUtil.class);
	
	public static String convertirNumeroEnLetras(double numero, String moneda) {
		try {
			
			String[] numeroEnLetras = NumeroUtil.agregarDecimales(numero, 2).split("\\" + Constante.SEPARADOR_DECIMAL);
			
			String entero  = numeroEnLetras[0];
			String decimal = numeroEnLetras[1];
			
			String letras = NumeroLetraUtil.convertirEnLetras(entero) + "y " + decimal + "/100 " + moneda;
			
			return letras.toUpperCase();
			
		} catch (Exception excepcion) {
			LOGGER.error("[NumeroUtil] - Ha ocurrido un error al usar el método convertirNumeroEnLetras.\n" +
				         "numero = " + numero, 
                         excepcion);
		}
		return ""; 
	}
	
	public static String agregarDecimales(double numero, int numeroDecimales){
		try {
			
			DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
			simbolos.setDecimalSeparator('.');
			
			StringBuffer formato = new StringBuffer();
			formato.append("0.");
			for(int posicion=0; posicion < numeroDecimales; posicion++) {
				formato.append("0");
			}
			
			DecimalFormat formateador = new DecimalFormat(formato.toString(), simbolos);
			
			return formateador.format(numero);
			
		}catch(Exception excepcion) {
			LOGGER.error("[NumeroUtil] - Ha ocurrido un error al usar el método agregarDecimales.\n"   
						    + "numero = " + numero   
						    + "numeroDecimales = " + numeroDecimales,
		                    excepcion);
		}
		return "0.00";
	}
	
	public static String convertirACadena(int numero) {
		return String.valueOf(numero);
	}
	
	public static String convertirACadena(double numero) {
		return String.valueOf(numero);
	}
	
}
