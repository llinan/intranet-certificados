package pe.certificados.web.dto;

import java.io.Serializable;


@SuppressWarnings("serial")
public class TipoCertificadoDto implements Serializable {

	private Integer id;
	private String nombre;
	private Integer estadoRegistro;

	
	
	public TipoCertificadoDto(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public TipoCertificadoDto() {
	}
	
	public TipoCertificadoDto(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}