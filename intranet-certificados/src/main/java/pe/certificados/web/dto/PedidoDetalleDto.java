package pe.certificados.web.dto;

import java.io.Serializable;

public class PedidoDetalleDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String ruc;
	private String razonSocial;
	private String direccionCompleta;	
	private String pais;
	private String departamento;	
	private String provincia;	
	private String distrito;
	private String nombreRepresentanteLegal;
	private String dniRepresentanteLegal;
	private String telefonoRepresentanteLegal;
	private String correoRepresentanteLegal;
	private String rucFacturacion;
	private String razonSocialFacturacion;
	private String direccionFacturacion;
	
	public PedidoDetalleDto() {
	}
	
	public PedidoDetalleDto(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getDireccionCompleta() {
		return direccionCompleta;
	}

	public void setDireccionCompleta(String direccionCompleta) {
		this.direccionCompleta = direccionCompleta;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getNombreRepresentanteLegal() {
		return nombreRepresentanteLegal;
	}

	public void setNombreRepresentanteLegal(String nombreRepresentanteLegal) {
		this.nombreRepresentanteLegal = nombreRepresentanteLegal;
	}

	public String getDniRepresentanteLegal() {
		return dniRepresentanteLegal;
	}

	public void setDniRepresentanteLegal(String dniRepresentanteLegal) {
		this.dniRepresentanteLegal = dniRepresentanteLegal;
	}

	public String getTelefonoRepresentanteLegal() {
		return telefonoRepresentanteLegal;
	}

	public void setTelefonoRepresentanteLegal(String telefonoRepresentanteLegal) {
		this.telefonoRepresentanteLegal = telefonoRepresentanteLegal;
	}

	public String getCorreoRepresentanteLegal() {
		return correoRepresentanteLegal;
	}

	public void setCorreoRepresentanteLegal(String correoRepresentanteLegal) {
		this.correoRepresentanteLegal = correoRepresentanteLegal;
	}

	public String getRucFacturacion() {
		return rucFacturacion;
	}

	public void setRucFacturacion(String rucFacturacion) {
		this.rucFacturacion = rucFacturacion;
	}

	public String getRazonSocialFacturacion() {
		return razonSocialFacturacion;
	}

	public void setRazonSocialFacturacion(String razonSocialFacturacion) {
		this.razonSocialFacturacion = razonSocialFacturacion;
	}

	public String getDireccionFacturacion() {
		return direccionFacturacion;
	}

	public void setDireccionFacturacion(String direccionFacturacion) {
		this.direccionFacturacion = direccionFacturacion;
	}
	
}