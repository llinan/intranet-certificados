package pe.certificados.web.dto;

import java.io.Serializable;


public class TiempoVigenciaDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String tiempo;
	
	public TiempoVigenciaDto(Integer id, String tiempo) {
		super();
		this.id = id;
		this.tiempo = tiempo;
	}

	public TiempoVigenciaDto() {
	}
	
	public TiempoVigenciaDto(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}

}