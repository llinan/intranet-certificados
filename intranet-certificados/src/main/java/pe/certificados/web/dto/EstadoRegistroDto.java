package pe.certificados.web.dto;

import java.io.Serializable;

import pe.certificados.core.type.EstadoPedido;
import pe.certificados.core.type.EstadoRegistro;

public class EstadoRegistroDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String nombre;
	private Integer estadoRegistro;
	
	public EstadoRegistroDto(EstadoRegistro estadoRegistro) {
		this.id = estadoRegistro.getId();
		this.nombre = estadoRegistro.getNombre();
	}
	
	public EstadoRegistroDto() {
	}
	
	public EstadoRegistroDto(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}