package pe.certificados.web.dto;
import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class UsuarioDto implements Serializable{

	private Integer id;
	
	private String login;
	
	private String clave;
	
	private String nombres;
	
	private String apellidos;
		
	private String ruc;
	
	private String dni;	

	private String correoElectronico;
	
	private int idTipoAliado;
	
	private String tipoAliado;
		
	private String telefono;
	
	private int cantidadPedidos;
	
	private String tipo;
	
	private int estadoRegistro;
	
	private String estadoRegistros;
	
	private String fechaRegistro;
	
	private Date fechaModificacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login == null ? "" : login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public int getIdTipoAliado() {
		return idTipoAliado;
	}

	public void setIdTipoAliado(int idTipoAliado) {
		this.idTipoAliado = idTipoAliado;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTipoAliado() {
		return tipoAliado;
	}

	public void setTipoAliado(String tipoAliado) {
		this.tipoAliado = tipoAliado;
	}

//	public String getEstadoRegistro() {
//		return estadoRegistro;
//	}
//
//	public void setEstadoRegistro(String estadoRegistro) {
//		this.estadoRegistro = estadoRegistro;
//	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getCantidadPedidos() {
		return cantidadPedidos;
	}

	public void setCantidadPedidos(int cantidadPedidos) {
		this.cantidadPedidos = cantidadPedidos;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(int estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getEstadoRegistros() {
		return estadoRegistros;
	}

	public void setEstadoRegistros(String estadoRegistros) {
		this.estadoRegistros = estadoRegistros;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

}
