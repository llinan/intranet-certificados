package pe.certificados.web.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


import pe.certificados.core.domain.PedidoDetalle;

public class PedidoDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer idTipoCertificado;
	private Integer idTiempoVigencia;
	private String monto;
	private Integer idUsuario;
	private Date fechaRegistro;
	private String numero;
	private List<PedidoDetalle> pedidosDetalle;
	private Integer idEstadoPedido;
	private Integer estadoRegistro;
	private String nombreTipoCertificado;
	private String nombreTiempoVigencia;
	private String RazonSocialTitular;
	private String rucTitular;
	private String representanteLegal;
	private String datosFacturacion;
	private String estadoPedido;
	private String nombreAliado;
	private String extensionImagen;
	
	public PedidoDto() {
	}
	
	public PedidoDto(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdTipoCertificado() {
		return idTipoCertificado;
	}

	public void setIdTipoCertificado(Integer idTipoCertificado) {
		this.idTipoCertificado = idTipoCertificado;
	}

	public Integer getIdTiempoVigencia() {
		return idTiempoVigencia;
	}

	public void setIdTiempoVigencia(Integer idTiempoVigencia) {
		this.idTiempoVigencia = idTiempoVigencia;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public List<PedidoDetalle> getPedidosDetalle() {
		return pedidosDetalle;
	}

	public void setPedidosDetalle(List<PedidoDetalle> pedidosDetalle) {
		this.pedidosDetalle = pedidosDetalle;
	}

	public Integer getIdEstadoPedido() {
		return idEstadoPedido;
	}

	public void setIdEstadoPedido(Integer idEstadoPedido) {
		this.idEstadoPedido = idEstadoPedido;
	}

	public Integer getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getNombreTipoCertificado() {
		return nombreTipoCertificado;
	}

	public void setNombreTipoCertificado(String nombreTipoCertificado) {
		this.nombreTipoCertificado = nombreTipoCertificado;
	}

	public String getNombreTiempoVigencia() {
		return nombreTiempoVigencia;
	}

	public void setNombreTiempoVigencia(String nombreTiempoVigencia) {
		this.nombreTiempoVigencia = nombreTiempoVigencia;
	}

	public String getRazonSocialTitular() {
		return RazonSocialTitular;
	}

	public void setRazonSocialTitular(String razonSocialTitular) {
		RazonSocialTitular = razonSocialTitular;
	}

	public String getRucTitular() {
		return rucTitular;
	}

	public void setRucTitular(String rucTitular) {
		this.rucTitular = rucTitular;
	}

	public String getRepresentanteLegal() {
		return representanteLegal;
	}

	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}

	public String getDatosFacturacion() {
		return datosFacturacion;
	}

	public void setDatosFacturacion(String datosFacturacion) {
		this.datosFacturacion = datosFacturacion;
	}

	public String getEstadoPedido() {
		return estadoPedido;
	}

	public void setEstadoPedido(String estadoPedido) {
		this.estadoPedido = estadoPedido;
	}

	public String getNombreAliado() {
		return nombreAliado;
	}

	public void setNombreAliado(String nombreAliado) {
		this.nombreAliado = nombreAliado;
	}

	public String getExtensionImagen() {
		return extensionImagen;
	}

	public void setExtensionImagen(String extensionImagen) {
		this.extensionImagen = extensionImagen;
	}

	
}