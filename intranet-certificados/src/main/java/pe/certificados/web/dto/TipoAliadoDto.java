package pe.certificados.web.dto;

import java.io.Serializable;



public class TipoAliadoDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String nombre;
	private Integer minimo;
	private Integer maximo;
	private Integer estadoRegistro;
//	private List<Usuario> usuarios;
	
	public TipoAliadoDto(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	
	public TipoAliadoDto() {
	}
	
	public TipoAliadoDto(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getMinimo() {
		return minimo;
	}

	public void setMinimo(Integer minimo) {
		this.minimo = minimo;
	}

	public Integer getMaximo() {
		return maximo;
	}

	public void setMaximo(Integer maximo) {
		this.maximo = maximo;
	}

//	public List<Usuario> getUsuarios() {
//		return usuarios;
//	}
//
//	public void setUsuarios(List<Usuario> usuarios) {
//		this.usuarios = usuarios;
//	}
	
	

}