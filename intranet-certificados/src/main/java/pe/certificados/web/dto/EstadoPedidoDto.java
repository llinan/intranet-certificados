package pe.certificados.web.dto;

import java.io.Serializable;

import pe.certificados.core.type.EstadoPedido;

public class EstadoPedidoDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String nombre;
	private Integer estadoRegistro;
	
	public EstadoPedidoDto(EstadoPedido estadoPedido) {
		this.id = estadoPedido.getId();
		this.nombre = estadoPedido.getNombre();
	}
	
	public EstadoPedidoDto() {
	}
	
	public EstadoPedidoDto(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstadoRegistro() {
		return this.estadoRegistro;
	}

	public void setEstadoRegistro(Integer estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}