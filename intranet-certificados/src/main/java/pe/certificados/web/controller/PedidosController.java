package pe.certificados.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pe.certificados.core.domain.ParametrosGenerales;
import pe.certificados.core.domain.Pedido;
import pe.certificados.core.domain.PedidoDetalle;
import pe.certificados.core.domain.Precio;
import pe.certificados.core.domain.Usuario;
import pe.certificados.core.service.ParametrosGeneralesService;
import pe.certificados.core.service.PedidoService;
import pe.certificados.core.service.PrecioService;
import pe.certificados.core.service.TiempoVigenciaService;
import pe.certificados.core.service.TipoAliadoService;
import pe.certificados.core.service.TipoCertificadoService;
import pe.certificados.core.service.UsuarioService;
import pe.certificados.core.type.EstadoPedido;
import pe.certificados.core.type.EstadoRegistro;
import pe.certificados.core.type.TipoAliadoEnum;
import pe.certificados.web.bean.RespuestaWeb;
import pe.certificados.web.dto.EstadoPedidoDto;
import pe.certificados.web.dto.PedidoDto;
import pe.certificados.web.dto.TiempoVigenciaDto;
import pe.certificados.web.dto.TipoCertificadoDto;
import pe.certificados.web.type.TipoRespuestaWeb;
import pe.certificados.web.util.ExceptionUtil;

import pe.certificados.web.util.RequestUtil;
import pe.certificados.web.util.WebUtil;

@Controller
public class PedidosController {

	public static final String PAGINA_LISTAR = "pedido/bandeja";
	
	private static final String PAGINA_REGISTRAR = "pedido/crear";
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	TipoAliadoService tipoAliadoService;
	
	@Autowired
	TipoCertificadoService tipoCertificadoService;
	
	@Autowired
	TiempoVigenciaService tiempoVigenciaService;
	
	@Autowired
	PrecioService precioService;
	
	@Autowired
	PedidoService pedidoService;
	
	@Autowired
	ParametrosGeneralesService parametrosGeneralesService;
	
	@RequestMapping(value = {"/pedido/bandeja"},  method = RequestMethod.GET)
	public String listar(Model model, HttpServletRequest request, HttpSession session) throws Exception {
		
		model.addAttribute("tiposEstadoPedido",obtenerEstadoPedido());
		model.addAttribute("tiposCertificado",obtenerTiposCertificado());


		return PAGINA_LISTAR;
	}
	
	@GetMapping(value="/pedido/listar")
	public @ResponseBody List<PedidoDto> listarCuentasCorreo() throws Exception {
		List<PedidoDto> listaPedidos = obtenerPedidosPorUsuarioDto();
		return listaPedidos;
	}
	 		
//	@GetMapping(value = {"/pedido/crear"})
	@RequestMapping(value = { "/pedido/crear" }, method = RequestMethod.POST)
	public String crear(Model model, HttpServletRequest request) {
		
		try {
			int idPedido = request.getParameter("idPedido") == null ? 0 : RequestUtil.parseInt(request.getParameter("idPedido"));
			
			model.addAttribute("tiposCertificado", obtenerTiposCertificadoNuevoPedido());
			model.addAttribute("tiemposVigencia", obtenerTiempoVigenciaNuevoPedido());
			model.addAttribute("pedido", new Pedido());
			model.addAttribute("detallePedido", new PedidoDetalle());

			if(idPedido != 0){
				Pedido pedido = pedidoService.obtenerPedido(idPedido);
				PedidoDetalle pedidoDetalle = pedidoService.obtenerDetallePedidoPorIdPedido(idPedido);
				request.getSession().setAttribute("pedido", pedido);
				model.addAttribute("pedido", pedido);
				if(pedidoDetalle != null){
					model.addAttribute("detallePedido",pedidoDetalle);
				}
				model.addAttribute("idPedido", pedido.getId());
//				model.addAttribute("pedido", pedido.getRutaImagen());
				model.addAttribute("numeroCorrelativo", pedido.getNumero());
				model.addAttribute("idEstadoPedido", pedido.getIdEstadoPedido());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return PAGINA_REGISTRAR;
	}
	
	@RequestMapping(value={"/pedido/obtenerPrecio"}, method = RequestMethod.GET)
    public ResponseEntity<?> obtenerNumeroCorrelativoPorIdSerie(HttpServletRequest request) {
		
		try {
			
			int idTiempoVigencia = RequestUtil.parseInt(request.getParameter("tiempo"));
			
			int tipoAliado = obtenerIdTipoAliadoParaObtenerMontoPago();
			
			Precio precio = precioService.obtenerPrecioPorTipoAliadoYTiempoVigencia(idTiempoVigencia, tipoAliado);
			
			RespuestaWeb respuestaWeb = new RespuestaWeb();
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			respuestaWeb.getParametros().put("precio", precio.getPrecio());
			
			return ResponseEntity.ok(respuestaWeb);
			
		} catch (Exception excepcion) {
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));
		}
		
	}
	
	@RequestMapping(value = "/pedido/registrar", method = RequestMethod.POST)
	public ResponseEntity<?> registrar(Model model, HttpServletRequest request) {
		try {
			int idTipoCertificado =  RequestUtil.parseInt(request.getParameter("idTipoCertificado"));
			int idTiempoVigencia =  RequestUtil.parseInt(request.getParameter("idTiempoVigencia"));
			Double monto = RequestUtil.parseDouble(request.getParameter("monto"));
			String nuevoPedido = request.getParameter("nuevoPedido");

			
			
			Pedido pedido = null;
			if("S".equals(nuevoPedido)){
				pedido = new Pedido();
				pedido.setIdTipoCertificado(idTipoCertificado);
				pedido.setIdTiempoVigencia(idTiempoVigencia);
				pedido.setMonto(monto);
				pedido.setIdUsuario(WebUtil.getUsuarioAutenticado().getId());
				pedido.setFechaRegistro(new Date());
				pedido.setIdEstadoPedido(EstadoPedido.EN_PROCESO.getId());
				pedido.setEstadoRegistro(EstadoRegistro.ACTIVO.getId());
				pedidoService.guardar(pedido);
			}else{
				pedido = new Pedido();
				pedido.setId(obtenerObjetoPedidoDtoDeSesion(request).getId());
				pedido.setIdTipoCertificado(idTipoCertificado);
				pedido.setIdTiempoVigencia(idTiempoVigencia);
				pedido.setMonto(monto);
				pedido.setIdUsuario(WebUtil.getUsuarioAutenticado().getId());
				pedido.setFechaRegistro(new Date());
				pedido.setIdEstadoPedido(EstadoPedido.EN_PROCESO.getId());
				pedido.setEstadoRegistro(EstadoRegistro.ACTIVO.getId());
				pedido.setNumero(obtenerObjetoPedidoDtoDeSesion(request).getNumero());
				pedidoService.guardar(pedido);
			}
			
			request.getSession().setAttribute("pedido",
					pedidoService.obtenerPedido(pedido.getId()));
			
			RespuestaWeb respuestaWeb = new RespuestaWeb();
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			respuestaWeb.getParametros().put("idPedido", pedido.getId());
			respuestaWeb.getParametros().put("numeroCorrelativo", pedido.getNumero());
			
			return ResponseEntity.ok(respuestaWeb);

		} catch (Exception e) {
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(e));
		}
		
	}
	
	@RequestMapping(value = "/pedido/detalle/registrar", method = RequestMethod.POST)
	public ResponseEntity<?> registrarDetalle(Model model, HttpServletRequest request) {
		try {
			
			String ruc = request.getParameter("ruc");
			String razonSocial = request.getParameter("razonSocial");
			String direccion = request.getParameter("direccion");
			String pais = request.getParameter("pais");
			String departamento = request.getParameter("departamento");
			String provincia = request.getParameter("provincia");
			String distrito = request.getParameter("distrito");
			String nombreRepresentante = request.getParameter("nombreRepresentante");
			String dniRepresentante = request.getParameter("dniRepresentante");
			String telefonoRepresentante = request.getParameter("telefonoRepresentante");
			String email = request.getParameter("email");
			String rucFacturacion = request.getParameter("rucFacturacion");
			String razonSocialFacturacion = request.getParameter("razonSocialFacturacion");
			String direccionFacturacion = request.getParameter("email");

			Pedido pedido = null;
			pedido = new Pedido();
			pedido.setId(obtenerObjetoPedidoDtoDeSesion(request).getId());
			pedido.setIdEstadoPedido(EstadoPedido.PENDIENTE_PAGAR.getId());
			for(int i = 0; i< 1;i++) {
				PedidoDetalle pedidoDetalle = new PedidoDetalle();
				pedidoDetalle.setRuc(ruc);
				pedidoDetalle.setRazonSocial(razonSocial);
				pedidoDetalle.setDireccionCompleta(direccion);
				pedidoDetalle.setPais(pais);
				pedidoDetalle.setDepartamento(departamento);
				pedidoDetalle.setProvincia(provincia);
				pedidoDetalle.setDistrito(distrito);
				pedidoDetalle.setNombreRepresentanteLegal(nombreRepresentante);
				pedidoDetalle.setDniRepresentanteLegal(dniRepresentante);
				pedidoDetalle.setTelefonoRepresentanteLegal(telefonoRepresentante);
				pedidoDetalle.setCorreoRepresentanteLegal(email);
				pedidoDetalle.setRucFacturacion(rucFacturacion);
				pedidoDetalle.setRazonSocialFacturacion(razonSocialFacturacion);
				pedidoDetalle.setDireccionFacturacion(direccionFacturacion);
				pedido.getPedidosDetalle().add(pedidoDetalle);
			}
			
			pedidoService.guardarDetallePedido(pedido);
			
			RespuestaWeb respuestaWeb = new RespuestaWeb();	
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			return ResponseEntity.ok(respuestaWeb);

		} catch (Exception e) {
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(e));
		}
		
	}
	
	@PostMapping("/pedido/subir")
    public ResponseEntity<?> singleFileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request, RedirectAttributes redirectAttributes) throws Exception {
		
        try {
        	ParametrosGenerales parametrosGenerales = parametrosGeneralesService.buscarPorId(2);
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get(parametrosGenerales.getDato1()+ File.separator + obtenerObjetoPedidoDtoDeSesion(request).getNumero()+"-"+file.getOriginalFilename());
            Files.write(path, bytes);

            pedidoService.agregarImagen(obtenerObjetoPedidoDtoDeSesion(request).getId(), parametrosGenerales.getDato1()+ File.separator +obtenerObjetoPedidoDtoDeSesion(request).getNumero()+"-"+file.getOriginalFilename());
            
            pedidoService.actualizarEstadoPedido(obtenerObjetoPedidoDtoDeSesion(request).getId(), EstadoPedido.PAGO_CONFIRMAR.getId());
            
            RespuestaWeb respuestaWeb = new RespuestaWeb();	
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			return ResponseEntity.ok(respuestaWeb);
			
        } catch (IOException e) {
        	redirectAttributes.addFlashAttribute("message",
                    "El archivo adjuntado excede el límite de tamaño. Intente otra vez.");
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(e));
        }catch (Exception e){
        	return ResponseEntity.badRequest().body(ExceptionUtil.controlar(e));
        }

	}
	
	private List<TipoCertificadoDto> obtenerTiposCertificado() throws Exception{
		List<TipoCertificadoDto> tiposCertificados = new ArrayList<TipoCertificadoDto>();
		
		tiposCertificados.add(new TipoCertificadoDto(0, "Todos"));
		tipoCertificadoService.listarTodos()
			.forEach( (tipoCertificado) -> {
				TipoCertificadoDto tipoCertificacionDto = new TipoCertificadoDto();
				tipoCertificacionDto.setId(tipoCertificado.getId());
				tipoCertificacionDto.setNombre(tipoCertificado.getNombre());
				tiposCertificados.add(tipoCertificacionDto);
			});
		
		return tiposCertificados;
	}
	
	private List<TiempoVigenciaDto> obtenerTiempoVigencia() throws Exception{
		List<TiempoVigenciaDto> tiemposVigencia = new ArrayList<TiempoVigenciaDto>();
		
		tiemposVigencia.add(new TiempoVigenciaDto(0, "Todos"));
		tiempoVigenciaService.listarTodos()
			.forEach( (tiempoVigencia) -> {
				TiempoVigenciaDto tiempoVigenciaDto = new TiempoVigenciaDto();
				tiempoVigenciaDto.setId(tiempoVigencia.getId());
				tiempoVigenciaDto.setTiempo(tiempoVigencia.getTiempo());
				tiemposVigencia.add(tiempoVigenciaDto);
			});
		
		return tiemposVigencia;
	}
	
	private List<TiempoVigenciaDto> obtenerTiempoVigenciaNuevoPedido() throws Exception{
		List<TiempoVigenciaDto> tiemposVigencia = new ArrayList<TiempoVigenciaDto>();
		
		tiempoVigenciaService.listarTodos()
			.forEach( (tiempoVigencia) -> {
				TiempoVigenciaDto tiempoVigenciaDto = new TiempoVigenciaDto();
				tiempoVigenciaDto.setId(tiempoVigencia.getId());
				tiempoVigenciaDto.setTiempo(tiempoVigencia.getTiempo());
				tiemposVigencia.add(tiempoVigenciaDto);
			});
		
		return tiemposVigencia;
	}
	
	private List<TipoCertificadoDto> obtenerTiposCertificadoNuevoPedido() throws Exception{
		List<TipoCertificadoDto> tiposCertificados = new ArrayList<TipoCertificadoDto>();
		
		tipoCertificadoService.listarTodos()
			.forEach( (tipoCertificado) -> {
				TipoCertificadoDto tipoCertificacionDto = new TipoCertificadoDto();
				tipoCertificacionDto.setId(tipoCertificado.getId());
				tipoCertificacionDto.setNombre(tipoCertificado.getNombre());
				tiposCertificados.add(tipoCertificacionDto);
			});
		
		return tiposCertificados;
	}
	
	private List<PedidoDto> obtenerPedidosPorUsuarioDto() throws Exception{
		List<PedidoDto> pedidosDto = new ArrayList<PedidoDto>();
		pedidoService.listarPedidoPorUsuario(WebUtil.getUsuarioAutenticado().getId()).forEach(pedido -> {
			try{
				PedidoDto pediodoDto = new PedidoDto();
				pediodoDto.setId(pedido.getId());
				pediodoDto.setNumero("N°PD " + pedido.getNumero());
				pediodoDto.setNombreTipoCertificado(obtenerNombreTipoCertificado(pedido.getIdTipoCertificado()));
				pediodoDto.setNombreTiempoVigencia(obtenerNombreTiempoVigencia(pedido.getIdTiempoVigencia()));
				for(PedidoDetalle pedidoDetalle : pedido.getPedidosDetalle()){
					pediodoDto.setRazonSocialTitular(pedidoDetalle.getRazonSocial());
					pediodoDto.setRucTitular(pedidoDetalle.getRuc());
					pediodoDto.setRepresentanteLegal(pedidoDetalle.getNombreRepresentanteLegal());
					pediodoDto.setDatosFacturacion(pedidoDetalle.getRucFacturacion()+"-"+pedidoDetalle.getRazonSocialFacturacion());
				}
				pediodoDto.setEstadoPedido(obtenerDescripcionEstadoPedido(pedido.getIdEstadoPedido()));
				pediodoDto.setIdEstadoPedido(pedido.getIdEstadoPedido());
				pedidosDto.add(pediodoDto);
			}catch (Exception e) {
				ExceptionUtil.controlar(e);
			}			
		});
		return pedidosDto;
	}
	
	private String obtenerNombreTipoCertificado(Integer id) throws Exception{
		return tipoCertificadoService.buscarPorId(id);
	}
	
	private String obtenerNombreTiempoVigencia(Integer id) throws Exception{
		return tiempoVigenciaService.buscarPorId(id);
	}
	
	private String obtenerDescripcionEstadoPedido(int idEstadoPedido) throws Exception{
		return EstadoPedido.obtener(idEstadoPedido).getNombre();
	}
	
	private List<EstadoPedidoDto> obtenerEstadoPedido() throws Exception{
		List<EstadoPedidoDto> estadoPedido = new ArrayList<EstadoPedidoDto>();
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.TODOS));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.EN_PROCESO));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.PENDIENTE_PAGAR));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.PAGO_CONFIRMAR));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.PAGO_CONFIRMADO));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.VALIDACION_COMPLETA));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.CERTIFICADO_EMITIDO));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.CERTIFICADO_DESCARGADO));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.FACTURA_EMITIDA));
		return estadoPedido;
	}

	private Pedido obtenerObjetoPedidoDtoDeSesion(HttpServletRequest request) throws Exception {
		if (request.getSession().getAttribute("pedido") == null) {
			return null;
		}
		return (Pedido) request.getSession().getAttribute("pedido");
	}
	
	private int obtenerIdTipoAliadoParaObtenerMontoPago() throws Exception{
		
		Usuario usuario = usuarioService.buscarPorId(WebUtil.getUsuarioAutenticado().getId());
		
		int cantidadPedidos = pedidoService.obtenerPedidosPorUsuarioEstadoPagoConfirmado(WebUtil.getUsuarioAutenticado().getId());

		if(usuario.getFlatCantidadPedidos() != null){
			cantidadPedidos = cantidadPedidos + usuario.getCantidadPedidos();
		}else{
			cantidadPedidos = cantidadPedidos + 1;
		}
		
		int tipoAliado = 0;
		
		if(cantidadPedidos == TipoAliadoEnum.BASIC.getMaximo()){
			tipoAliado = TipoAliadoEnum.BRONCE.getId();
		}else if(cantidadPedidos == TipoAliadoEnum.BRONCE.getMaximo()){
			tipoAliado = TipoAliadoEnum.PLATA.getId();
		}else if(cantidadPedidos == TipoAliadoEnum.PLATA.getMaximo()){
			tipoAliado =  TipoAliadoEnum.ORO.getId();
		}else if(cantidadPedidos == TipoAliadoEnum.ORO.getMaximo()){
			tipoAliado =  TipoAliadoEnum.PLATINIUM.getId();
		}else if(cantidadPedidos == TipoAliadoEnum.PLATINIUM.getMaximo()){
			tipoAliado =  TipoAliadoEnum.VIP.getId();
		}else{
			tipoAliado= usuario.getTipoAliado().getId();
		}
		return tipoAliado;
	}
}
