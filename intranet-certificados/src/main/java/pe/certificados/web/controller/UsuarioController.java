package pe.certificados.web.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pe.certificados.core.domain.ParametrosGenerales;
import pe.certificados.core.domain.ParametrosServidorCorreo;
import pe.certificados.core.domain.TipoAliado;
import pe.certificados.core.domain.Usuario;
import pe.certificados.core.service.ParametrosGeneralesService;
import pe.certificados.core.service.ParametrosServidorCorreoService;
import pe.certificados.core.service.PedidoService;
import pe.certificados.core.service.TipoAliadoService;
import pe.certificados.core.service.UsuarioService;
import pe.certificados.core.type.EstadoPedido;
import pe.certificados.core.type.EstadoRegistro;
import pe.certificados.core.type.TipoAliadoEnum;
import pe.certificados.web.bean.Email;
import pe.certificados.web.bean.EmailConfig;
import pe.certificados.web.bean.EmailTemplate;
import pe.certificados.web.bean.RespuestaWeb;
import pe.certificados.web.dto.EmpresaDto;
import pe.certificados.web.dto.EstadoPedidoDto;
import pe.certificados.web.dto.EstadoRegistroDto;
import pe.certificados.web.dto.TiempoVigenciaDto;
import pe.certificados.web.dto.TipoAliadoDto;
import pe.certificados.web.dto.TipoCertificadoDto;
import pe.certificados.web.dto.UsuarioDto;
import pe.certificados.web.type.TipoRespuestaWeb;
import pe.certificados.web.util.ClaveUtil;
import pe.certificados.web.util.EmailUtil;
import pe.certificados.web.util.EncriptaUtil;
import pe.certificados.web.util.ExceptionUtil;
import pe.certificados.web.util.FechaUtil;
import pe.certificados.web.util.RequestUtil;
import pe.certificados.web.util.WebUtil;

@Controller
public class UsuarioController {

	public static final String PAGINA_LISTAR = "usuario/listar";
	
	private static final String PAGINA_REGISTRAR = "usuario/registrar";
	
	private static final String PAGINA_EDITAR = "usuario/editar";
	
	private static final String PAGINA_PERFIL = "usuario/perfil";
	
	private static final String PAGINA_PERFIL_ADMIN = "usuario/perfil-administrador";

	private static final String PAGINA_PERFIL_EDITAR = "usuario/editar-perfil";
	
	private static final String PAGINA_PERFIL_EDITAR_ADMIN = "usuario/editar-perfil-admin";

	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	TipoAliadoService tipoAliadoService;
	
	@Autowired
	ParametrosServidorCorreoService parametrosServidorCorreoService;
	
	@Autowired
	ParametrosGeneralesService parametrosGeneralesService;
	
	@Autowired
	PedidoService pedidoService;
	
	@RequestMapping(value = {"/usuario/bandeja"},  method = RequestMethod.GET)
	public String listar(Model model, HttpServletRequest request, HttpSession session) throws Exception {
			
		model.addAttribute("tiposAliado",obtenerTiposCertificado());
		model.addAttribute("estadosRegistro",obtenerEstadoRegistro());
		return PAGINA_LISTAR;
	}
	
	@RequestMapping(value = {"/usuario/perfil"},  method = RequestMethod.GET)
	public String verPerfil(Model model, HttpServletRequest request, HttpSession session) {
		
		try{
			Usuario usuario = usuarioService.buscarPorId(WebUtil.getUsuarioAutenticado().getId());
			
			Integer cantidadPedidosPorUsuario = pedidoService.obtenerPedidosPorUsuarioEstadoPagoConfirmado(usuario.getId());
			
			model.addAttribute("nombreCompleto", usuario.getNombres()+" "+usuario.getApellidos());
			
			if(usuario.getFlatCantidadPedidos() != null){
				model.addAttribute("cantidadPedidos", usuario.getCantidadPedidos()+cantidadPedidosPorUsuario);
				model.addAttribute("tipoAliado", usuario.getTipoAliado().getNombre());	
			}else{
				if(cantidadPedidosPorUsuario == 0){
					model.addAttribute("cantidadPedidos", 1);
					model.addAttribute("tipoAliado", "-");
				}else{
					model.addAttribute("cantidadPedidos", cantidadPedidosPorUsuario+1);
					model.addAttribute("tipoAliado", usuario.getTipoAliado().getNombre());	
				}
			}
			
		
			model.addAttribute("usuario", usuario);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return PAGINA_PERFIL;
	}
	
	@RequestMapping(value = {"/usuario/administrador/perfil"},  method = RequestMethod.GET)
	public String verPerfilAdministrador(Model model, HttpServletRequest request, HttpSession session) {
		
		try{
			Usuario usuario = usuarioService.buscarPorId(WebUtil.getUsuarioAutenticado().getId());
			
			model.addAttribute("nombreCompleto", usuario.getNombres()+" "+usuario.getApellidos());
			model.addAttribute("usuario", usuario);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return PAGINA_PERFIL_ADMIN;
	}
	
	@RequestMapping(value = {"/usuario/perfil/guardar"},  method = RequestMethod.POST)
	public String editarPerfilAliado(Model model, HttpServletRequest request, HttpSession session, UsuarioDto usuarioDto) {
		
		try{
			
			usuarioService.guardar(obtenerUsuario(usuarioDto));
			model.addAttribute("mostrarNotificacionExito", true);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/usuario/perfil";
	}
	
	@RequestMapping(value = {"/usuario/perfil/admin/guardar"},  method = RequestMethod.POST)
	public String editarPerfilAdministrador(Model model, HttpServletRequest request, HttpSession session, UsuarioDto usuarioDto) {
		
		try{
			
			usuarioService.guardar(obtenerUsuarioAdministrador(usuarioDto));
			model.addAttribute("mostrarNotificacionExito", true);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/usuario/administrador/perfil";
	}
	
	@GetMapping(value="/usuario/listar")
	public @ResponseBody List<UsuarioDto> listarCuentasCorreo() throws Exception {
		List<UsuarioDto> listaUsuarios = obtenerUsuariosDto();
		return listaUsuarios;
	}
	
	@GetMapping(value = {"/usuario/registrar"})
	public String registrar(Model model, HttpServletRequest request) {
		
		try {
			model.addAttribute("usuario", new Usuario());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return PAGINA_REGISTRAR;
	}
	
	@GetMapping(value={"/usuario/editar/{id}"})
    public String editar(Model model, @PathVariable(required = false, name = "id") Integer id) {
		
		try {
			
			model.addAttribute("usuario", obtenerUsuarioDto(id));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return PAGINA_EDITAR;	
	}
	
	@GetMapping(value={"/perfil/editar/{id}"})
    public String editarPerfil(Model model, @PathVariable(required = false, name = "id") Integer id) {
		
		try {
			
			model.addAttribute("usuario", obtenerUsuarioDto(id));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return PAGINA_PERFIL_EDITAR;	
	}
	
	@GetMapping(value={"/perfil/admin/{id}"})
    public String editarPerfilAdministrador(Model model, @PathVariable(required = false, name = "id") Integer id) {
		
		try {
			
			model.addAttribute("usuario", obtenerUsuarioDto(id));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return PAGINA_PERFIL_EDITAR_ADMIN;	
//		return "redirect:/usuario/editar-perfil-administrador";

	}
	
	@RequestMapping(value={"/usuario/guardar"}, method = RequestMethod.POST)
	public String guardar(Model model, UsuarioDto usuarioDto, RedirectAttributes atributos, HttpSession session){
		
		try {
			
//			if( usuario.getId() == null ) {
//				mensaje = "El usuario se registró exitosamente.";	
//				usuario.setClave(ClaveUtil.encriptarClave(usuario.getClave()));
//			}else {
//				mensaje = "El usuario se actualizó exitosamente.";
//				if(usuario.getClave()!=null && !"".equals(usuario.getClave())) {
//					usuario.setClave(ClaveUtil.encriptarClave(usuario.getClave()));
//				}
//			}
			usuarioService.guardar(obtenerUsuario(usuarioDto));
			model.addAttribute("mostrarNotificacionExito", true);

//			model.addAttribute("usuarios", usuarioService.listarTodos());			
			
		} catch (Exception e) {	
			e.printStackTrace();			
		} 
		
		return "redirect:/usuario/bandeja";
	}
	
	@RequestMapping(value = "/usuario/habilitarInhabilitar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> habilitarInhabilitar(HttpServletRequest request, Model model) {
		try {
			int id = request.getParameter("idUsuario") == null ? 0 : Integer.parseInt(request.getParameter("idUsuario"));
			int estadoRegistro = request.getParameter("estado").equals("ACTIVO") ? 0 : 1;
			
			usuarioService.habilitarInhabilitar(id, estadoRegistro);
			
			model.addAttribute("mostrarNotificacionExito", true);
			
			RespuestaWeb respuestaWeb = new RespuestaWeb();
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			
			return ResponseEntity.ok(respuestaWeb);

		} catch (Exception excepcion) {
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));

		}
	}
	
	@RequestMapping(value = { "/usuario/existe/dni" }, method = RequestMethod.GET)
	public @ResponseBody String existeUsuarioDni(HttpServletRequest request) {
		
		boolean esValidoParaRegistrar = true;
		try {
			
			String dni  = RequestUtil.parseString(request.getParameter("dni"));			
			Usuario usuario = usuarioService.validacionDniUsuario(dni);
				if ( usuario != null ) {
				esValidoParaRegistrar = false;
				}
						
			return String.valueOf(esValidoParaRegistrar);
			
		} catch (Exception excepcion) {
			ExceptionUtil.controlar(excepcion);
			return String.valueOf(false);
		}
		
	}
	
	@RequestMapping(value = { "/usuario/existe/ruc" }, method = RequestMethod.GET)
	public @ResponseBody String existeUsuarioRuc(HttpServletRequest request) {
		
		boolean esValidoParaRegistrar = true;
		try {
			
			String ruc  = RequestUtil.parseString(request.getParameter("ruc"));			
			Usuario usuario = usuarioService.validacionRucUsuario(ruc);
				if ( usuario != null ) {
				esValidoParaRegistrar = false;
				}
				
			
			return String.valueOf(esValidoParaRegistrar);
			
		} catch (Exception excepcion) {
			ExceptionUtil.controlar(excepcion);
			return String.valueOf(false);
		}
		
	}
	
	@RequestMapping(value = { "/usuario/existe/editar/dni" }, method = RequestMethod.GET)
	public @ResponseBody String existeUsuarioDniEditar(HttpServletRequest request) {
		
		boolean esValidoParaRegistrar = false;
		try {
			
			int idUsuario =RequestUtil.parseInt(request.getParameter("id"));
			String dni  = RequestUtil.parseString(request.getParameter("dni"));			
			
			Usuario usuario = usuarioService.validacionDniUsuario(dni);
				if ( usuario == null || usuario.getId() == idUsuario ) {
					esValidoParaRegistrar = true;
				}
				
			
			return String.valueOf(esValidoParaRegistrar);
			
		} catch (Exception excepcion) {
			ExceptionUtil.controlar(excepcion);
			return String.valueOf(false);
		}
		
	}
	
	@RequestMapping(value = { "/usuario/existe/editar/ruc" }, method = RequestMethod.GET)
	public @ResponseBody String existeUsuarioRucEditar(HttpServletRequest request) {
		
		boolean esValidoParaRegistrar = false;
		try {
			
			int idUsuario =RequestUtil.parseInt(request.getParameter("id"));
			String ruc  = RequestUtil.parseString(request.getParameter("ruc"));			
			Usuario usuario = usuarioService.validacionRucUsuario(ruc);
			
				if ( usuario == null || usuario.getId() == idUsuario) {
					esValidoParaRegistrar = true;
				}
				
			
			return String.valueOf(esValidoParaRegistrar);
			
		} catch (Exception excepcion) {
			ExceptionUtil.controlar(excepcion);
			return String.valueOf(false);
		}
		
	}
			
	/**Llinan**/
	@RequestMapping(value="/usuario/generarClave", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> enviarNotificacionClave(HttpServletRequest request, Model model) {
		
		try {
			int id = request.getParameter("idUsuario") == null ? 0 : Integer.parseInt(request.getParameter("idUsuario"));

			Usuario usuario = usuarioService.buscarPorId(id);
			
			usuarioService.enviarNotificacionCorreoElectronico(usuario);
						
			RespuestaWeb respuestaWeb = new RespuestaWeb();
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			respuestaWeb.getParametros().put("mensaje", "Se le ha enviado una notificación para la generación de clave.");

			return ResponseEntity.ok(respuestaWeb);
			
		} catch (Exception excepcion) {
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));
		}
		
	}
	/**Llinan**/

	@RequestMapping(value="/usuario/guardarClave", method=RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> guardarClaveColaborador(HttpServletRequest request, Model model){
        try {
        	//TODO Seguridad: revisar si es posible que el usuario altere el parametro encriptado "p" por otro
        	String id = EncriptaUtil.desencriptarIdUsuario(request.getParameter("id"));
			String clave = request.getParameter("clave");
			
			Usuario usuario = usuarioService.buscarPorId(Integer.parseInt(id));
			
			if(usuario != null) {
				usuarioService.crearClaveUsuario(Integer.parseInt(id), ClaveUtil.encriptarClave(clave));
				WebUtil.setSesionUsuarioAutenticado(usuario);
			}
		
			RespuestaWeb respuestaWeb = new RespuestaWeb();
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			
			return ResponseEntity.ok(respuestaWeb);
			
		} catch (Exception excepcion) {
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));
		}
    }
	
	/**Llinan**/
	@RequestMapping(value="/usuario/recuperarClave", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> enviarNotificacionClaveRecuperarClave(HttpServletRequest request, Model model) {
		
		try {
			String login = request.getParameter("login") == null ? "" : request.getParameter("login");

			Usuario usuario = usuarioService.buscarPorLogin(login);

			if(usuario == null){
				RespuestaWeb respuestaWeb = new RespuestaWeb();
				respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.ERROR);
				return ResponseEntity.ok(respuestaWeb);
			}
			
			usuarioService.enviarCorreoElectronicoRecuperarClave(usuario);
						
			RespuestaWeb respuestaWeb = new RespuestaWeb();
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);

			return ResponseEntity.ok(respuestaWeb);
			
		} catch (Exception excepcion) {
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));
		}
		
	}
	/**Llinan**/
	
	private List<UsuarioDto> obtenerUsuariosDto() throws Exception{
		List<UsuarioDto> usuariosDto = new ArrayList<UsuarioDto>();
		usuarioService.listarTodos().forEach(usuario -> {
			try {
			UsuarioDto usuarioDto = new UsuarioDto();
			usuarioDto.setId(usuario.getId());
			usuarioDto.setLogin(usuario.getLogin());
			usuarioDto.setNombres(usuario.getNombres()+" "+usuario.getApellidos());
			usuarioDto.setRuc(usuario.getRuc());
			usuarioDto.setDni(usuario.getDni());
			usuarioDto.setCorreoElectronico(usuario.getCorreoElectronico());
			usuarioDto.setTelefono(usuario.getTelefono());
			int cantidadPedidos = pedidoService.obtenerPedidosPorUsuarioEstadoPagoConfirmado(usuario.getId());
			if(usuario.getFlatCantidadPedidos() != null){
				cantidadPedidos = usuario.getCantidadPedidos() + pedidoService.obtenerPedidosPorUsuarioEstadoPagoConfirmado(usuario.getId());
				usuarioDto.setTipoAliado(usuario.getTipoAliado().getNombre());
				usuarioDto.setCantidadPedidos(cantidadPedidos);
			}else{
				if(cantidadPedidos == 0){
					usuarioDto.setTipoAliado("-");
					usuarioDto.setCantidadPedidos(1);
				}else{
					usuarioDto.setTipoAliado(usuario.getTipoAliado().getNombre());
					usuarioDto.setCantidadPedidos(cantidadPedidos+1);
				}
			}
			usuarioDto.setEstadoRegistros( (usuario.getEstadoRegistro()) == 1 ? "ACTIVO" : "INACTIVO");
			usuariosDto.add(usuarioDto);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		});
		return usuariosDto;
	}
	
	private Usuario obtenerUsuario(UsuarioDto usuarioDto) throws Exception{
		Usuario usuario = new Usuario();
		usuario.setId(usuarioDto.getId());
		usuario.setNombres(usuarioDto.getNombres());
		usuario.setApellidos(usuarioDto.getApellidos());
		usuario.setLogin(usuarioDto.getCorreoElectronico());
		usuario.setCorreoElectronico(usuarioDto.getCorreoElectronico());
		usuario.setTelefono(usuarioDto.getTelefono());
		usuario.setRuc(usuarioDto.getRuc());
		usuario.setDni(usuarioDto.getDni());
		usuario.setTipo("C");
		usuario.setEstadoRegistro(EstadoRegistro.ACTIVO.getId());
//		if(usuarioDto.getCantidadPedidos() > 1 ) {
//			usuario.setCantidadPedidos(usuarioDto.getCantidadPedidos());
//		}else{
//			usuario.setCantidadPedidos(1);
//		}
		if(usuarioDto.getIdTipoAliado() != 0){
			usuario.setTipoAliado(obtenerTipoAliadoPorId(usuarioDto.getIdTipoAliado()));
		}else{
			usuario.setTipoAliado(obtenerTipoAliadoPorId(TipoAliadoEnum.BASIC.getId()));
		}
//		usuario.setTipoAliado(obtenerTipoAliadoPorId(TipoAliadoEnum.BASIC.getId()));
		if(usuarioDto.getFechaRegistro() == null){
			usuario.setFechaRegistro(new Date());
		}else{
		    Date date1=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(usuarioDto.getFechaRegistro());  
			usuario.setFechaRegistro(date1);
			usuario.setFechaModificacion(new Date());
		}
		return usuario;
	}
	
	private Usuario obtenerUsuarioAdministrador(UsuarioDto usuarioDto) throws Exception{
		Usuario usuario = new Usuario();
		usuario.setId(usuarioDto.getId());
		usuario.setNombres(usuarioDto.getNombres());
		usuario.setApellidos(usuarioDto.getApellidos());
		usuario.setLogin(usuarioDto.getCorreoElectronico());
		usuario.setCorreoElectronico(usuarioDto.getCorreoElectronico());
		usuario.setClave(ClaveUtil.encriptarClave(usuarioDto.getClave()));
		usuario.setTelefono(usuarioDto.getTelefono());
		usuario.setRuc(usuarioDto.getRuc());
		usuario.setDni(usuarioDto.getDni());
		usuario.setTipo("A");
		usuario.setEstadoRegistro(EstadoRegistro.ACTIVO.getId());
//		if(usuarioDto.getCantidadPedidos() > 1 ) {
//			usuario.setCantidadPedidos(usuarioDto.getCantidadPedidos());
//		}else{
//			usuario.setCantidadPedidos(1);
//		}
		if(usuarioDto.getIdTipoAliado() != 0){
			usuario.setTipoAliado(obtenerTipoAliadoPorId(usuarioDto.getIdTipoAliado()));
		}else{
			usuario.setTipoAliado(obtenerTipoAliadoPorId(TipoAliadoEnum.BASIC.getId()));
		}
//		usuario.setTipoAliado(obtenerTipoAliadoPorId(TipoAliadoEnum.BASIC.getId()));
		return usuario;
	}
	
	private TipoAliado obtenerTipoAliadoPorId(int id) throws Exception{
		return tipoAliadoService.buscarPorId(id);
	}
	
	private List<TipoAliadoDto> obtenerTiposCertificado() throws Exception{
		List<TipoAliadoDto> tiposAliadosDto = new ArrayList<TipoAliadoDto>();
		
		tiposAliadosDto.add(new TipoAliadoDto(0, "Todos"));
		tipoAliadoService.listarTodos()
			.forEach( (tipoAliado) -> {
				TipoAliadoDto tipoAliadoDto = new TipoAliadoDto();
				tipoAliadoDto.setId(tipoAliado.getId());
				tipoAliadoDto.setNombre(tipoAliado.getNombre());
				tiposAliadosDto.add(tipoAliadoDto);
			});
		
		return tiposAliadosDto;
	}
	
	private UsuarioDto obtenerUsuarioDto(int id) throws Exception{
		Usuario usuario = usuarioService.buscarPorId(id);
		UsuarioDto usuarioDto = new UsuarioDto();
		usuarioDto.setId(usuario.getId());
		usuarioDto.setNombres(usuario.getNombres());
		usuarioDto.setApellidos(usuario.getApellidos());
		usuarioDto.setLogin(usuario.getCorreoElectronico());
		usuarioDto.setClave(usuario.getClave());
		usuarioDto.setCorreoElectronico(usuario.getCorreoElectronico());
		usuarioDto.setTelefono(usuario.getTelefono());
		usuarioDto.setRuc(usuario.getRuc());
		usuarioDto.setDni(usuario.getDni());
		usuarioDto.setTipo("C");
		usuarioDto.setEstadoRegistro(usuario.getEstadoRegistro());
//		usuarioDto.setCantidadPedidos(usuario.getCantidadPedidos());
		usuarioDto.setIdTipoAliado(usuario.getTipoAliado().getId());
		usuarioDto.setFechaRegistro(FechaUtil.convertirACadena(usuario.getFechaRegistro(), "dd/MM/yyyy HH:mm:ss"));
		return usuarioDto;
	}
	
	private List<EstadoRegistroDto> obtenerEstadoRegistro() throws Exception{
		List<EstadoRegistroDto> estadoRegistro = new ArrayList<EstadoRegistroDto>();
		estadoRegistro.add(new EstadoRegistroDto(EstadoRegistro.TODOS));
		estadoRegistro.add(new EstadoRegistroDto(EstadoRegistro.ACTIVO));
		estadoRegistro.add(new EstadoRegistroDto(EstadoRegistro.INACTIVO));
		return estadoRegistro;
	}
}
