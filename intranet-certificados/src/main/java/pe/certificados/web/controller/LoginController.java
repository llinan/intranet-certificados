package pe.certificados.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.certificados.core.domain.Usuario;
import pe.certificados.core.service.UsuarioService;
import pe.certificados.web.util.ClaveUtil;
import pe.certificados.web.util.EncriptaUtil;
import pe.certificados.web.util.WebUtil;


@Controller
public class LoginController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	
	private static final String PAGINA_LOGIN = "login-nuevo";
	
	private static final String PAGINA_GENERAR_CLAVE = "usuario/generar-contrasenia";

	
	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping(value = {"/", "/login"})
	public String inicio(Model model, HttpServletRequest request) {
		
		Integer idUsuario = 0;
		String url = PAGINA_LOGIN;
		
		try {
			
			String idUsuarioEncriptado = request.getParameter("c");
			
			idUsuario = request.getParameter("c")==null ? 0 : Integer.parseInt(EncriptaUtil.desencriptarIdUsuario(request.getParameter("c")));				
			
			if(idUsuario>0){
				LOGGER.info("[INTRANET-CERTIFICADOS] - idUsuarioDesencrptado: " + idUsuario);
				url = "redirect:/login/generarClave?p=" + idUsuarioEncriptado;
				LOGGER.info("[INTRANET-CERTIFICADOS] - redirect URL: " + url);
			}
			
		}catch (Exception e) {
			 e.printStackTrace();
		}
		return url;
	}
	
	@RequestMapping(value = { "/login/autenticarUsuario" }, method = RequestMethod.POST)
	public @ResponseBody String autenticarUsuario(HttpServletRequest request) {
		
		try {
			
			String login = request.getParameter("usuario");
			String clave = ClaveUtil.encriptarClave(request.getParameter("clave"));
			
			System.out.println("Inicio de Sesion - " + "LOGIN : " + login + " - "+"CLAVE : " + request.getParameter("clave"));
			
			Usuario usuario = usuarioService.iniciarSesion(login, clave);
			
			if( usuario != null) {
				WebUtil.setSesionUsuarioAutenticado(usuario);
				return Boolean.TRUE.toString();
			}
			return Boolean.FALSE.toString();
			
		} catch (Exception excepcion) {
			excepcion.printStackTrace();
			return Boolean.FALSE.toString();
		}
		
	}
	
	@RequestMapping(value = { "/login/iniciarSesion" }, method = RequestMethod.POST)
	public String iniciarSesion(HttpServletRequest request) throws Exception {
		
		String pagina= "";
		
		if(WebUtil.getUsuarioAutenticado().getTipo().equals("A")){
			pagina ="redirect:/usuario/administrador/perfil";
		}else{
			pagina ="redirect:/usuario/perfil";
		}
		
		return pagina;
	}
	
	
	@RequestMapping(value = { "/login/cerrarSesion" }, method = RequestMethod.GET)
	public String cerrarSesion(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/" + PAGINA_LOGIN;
	}
	
	@RequestMapping(value="/login/generarClave", method=RequestMethod.GET)
    public String cargarPaginaGenerarClave(HttpServletRequest request, Model model){
		String url = PAGINA_GENERAR_CLAVE;

		try {
			String login = "";
			String loginEncriptado = request.getParameter("p");
			//TODO: revisar porque obtiene el parametro con salto de linea
			loginEncriptado = loginEncriptado.replace("\n", "").replace("\r", "");
			try {
				login = EncriptaUtil.desencriptarIdUsuario(loginEncriptado);
				
			} catch (Exception excepcion) {
				login = "";
			}
			
			if("".equals(login)){
				url = "redirect:/" + PAGINA_LOGIN;
			}else{
				Usuario usuario = usuarioService.buscarPorId(Integer.parseInt(login));
				if(usuario!=null && usuario.getClave()==null){
					model.addAttribute("p", loginEncriptado);
					model.addAttribute("login",usuario.getLogin());
				}else{
					url = "redirect:/" + PAGINA_LOGIN;			
				}
			}
			
		} catch (Exception excepcion) {
			excepcion.printStackTrace();
		}
        return url;
    }
	
}
