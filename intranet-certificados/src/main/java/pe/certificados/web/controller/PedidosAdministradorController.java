package pe.certificados.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.certificados.core.bean.ArchivoRespuesta;
import pe.certificados.core.domain.Pedido;
import pe.certificados.core.domain.PedidoDetalle;
import pe.certificados.core.domain.Precio;
import pe.certificados.core.service.PedidoService;
import pe.certificados.core.service.PrecioService;
import pe.certificados.core.service.TiempoVigenciaService;
import pe.certificados.core.service.TipoAliadoService;
import pe.certificados.core.service.TipoCertificadoService;
import pe.certificados.core.service.UsuarioService;
import pe.certificados.core.type.EstadoPedido;
import pe.certificados.core.type.EstadoRegistro;
import pe.certificados.web.bean.RespuestaWeb;
import pe.certificados.web.dto.EstadoPedidoDto;
import pe.certificados.web.dto.PedidoDto;
import pe.certificados.web.dto.TiempoVigenciaDto;
import pe.certificados.web.dto.TipoCertificadoDto;
import pe.certificados.web.type.TipoRespuestaWeb;
import pe.certificados.web.util.ExceptionUtil;

import pe.certificados.web.util.RequestUtil;
import pe.certificados.web.util.WebUtil;

@Controller
public class PedidosAdministradorController {

	public static final String PAGINA_LISTAR = "pedido-administrador/bandeja";
	
	private static final String PAGINA_REGISTRAR = "pedido/crear";
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	TipoAliadoService tipoAliadoService;
	
	@Autowired
	TipoCertificadoService tipoCertificadoService;
	
	@Autowired
	TiempoVigenciaService tiempoVigenciaService;
	
	@Autowired
	PrecioService precioService;
	
	@Autowired
	PedidoService pedidoService;
	
	@RequestMapping(value = {"/pedido/admin/bandeja"},  method = RequestMethod.GET)
	public String listar(Model model, HttpServletRequest request, HttpSession session) throws Exception {

		Integer idUsuario = RequestUtil.parseInt(request.getParameter("idUsuario"));
		
		model.addAttribute("idUsuario", idUsuario);
		model.addAttribute("tiposEstadoPedido",obtenerEstadoPedido());
		model.addAttribute("tiposCertificado",obtenerTiposCertificado());

		
		return PAGINA_LISTAR;
	}
	
	@GetMapping(value="/pedido/admin/listar")
	public @ResponseBody List<PedidoDto> listarCuentasCorreo(HttpServletRequest request) throws Exception {
		
		Integer idUsuario = RequestUtil.parseInt(request.getParameter("idUsuario"));
		List<PedidoDto> listaPedidos = null;
				
		if(idUsuario != 0 ) {
			listaPedidos = listarPedidoPorIdUsuario(idUsuario);
		}else {
			listaPedidos = listarTodos();
		}
	
		return listaPedidos;
	}
	
	private List<PedidoDto> listarTodos() throws Exception{
		List<PedidoDto> pedidosDto = new ArrayList<PedidoDto>();
		pedidoService.listarTodos().forEach(pedido -> {
			try{
				PedidoDto pediodoDto = new PedidoDto();
				pediodoDto.setId(pedido.getId());
				pediodoDto.setNumero("N°PD " + pedido.getNumero());
				pediodoDto.setNombreAliado(obtenerNombreApellidoUsuario(pedido.getIdUsuario()));
				pediodoDto.setNombreTipoCertificado(obtenerNombreTipoCertificado(pedido.getIdTipoCertificado()));
				pediodoDto.setNombreTiempoVigencia(obtenerNombreTiempoVigencia(pedido.getIdTiempoVigencia()));
				for(PedidoDetalle pedidoDetalle : pedido.getPedidosDetalle()){
					pediodoDto.setRazonSocialTitular(pedidoDetalle.getRazonSocial());
					pediodoDto.setRucTitular(pedidoDetalle.getRuc());
					pediodoDto.setRepresentanteLegal(pedidoDetalle.getNombreRepresentanteLegal()+"-"+pedidoDetalle.getDniRepresentanteLegal());
//					pediodoDto.setDatosFacturacion(pedidoDetalle.getRucFacturacion()+"-"+pedidoDetalle.getRazonSocialFacturacion());
					pediodoDto.setDatosFacturacion(pedidoDetalle.getRuc()+"-"+pedidoDetalle.getRazonSocial());
				}
				pediodoDto.setEstadoPedido(obtenerDescripcionEstadoPedido(pedido.getIdEstadoPedido()));
				pediodoDto.setExtensionImagen(FilenameUtils.getExtension(pedido.getRutaImagen()));
				pedidosDto.add(pediodoDto);
			}catch (Exception e) {
				ExceptionUtil.controlar(e);
			}			
		});
		return pedidosDto;
	}
	
	private List<PedidoDto> listarPedidoPorIdUsuario(Integer id) throws Exception{
		List<PedidoDto> pedidosDto = new ArrayList<PedidoDto>();
		pedidoService.listarPedidoPorUsuario(id).forEach(pedido -> {
			try{
				PedidoDto pediodoDto = new PedidoDto();
				pediodoDto.setId(pedido.getId());
				pediodoDto.setNumero("N°PD " + pedido.getNumero());
				pediodoDto.setNombreAliado(obtenerNombreApellidoUsuario(pedido.getIdUsuario()));
				pediodoDto.setNombreTipoCertificado(obtenerNombreTipoCertificado(pedido.getIdTipoCertificado()));
				pediodoDto.setNombreTiempoVigencia(obtenerNombreTiempoVigencia(pedido.getIdTiempoVigencia()));
				for(PedidoDetalle pedidoDetalle : pedido.getPedidosDetalle()){
					pediodoDto.setRazonSocialTitular(pedidoDetalle.getRazonSocial());
					pediodoDto.setRucTitular(pedidoDetalle.getRuc());
					pediodoDto.setRepresentanteLegal(pedidoDetalle.getNombreRepresentanteLegal());
					pediodoDto.setDatosFacturacion(pedidoDetalle.getRucFacturacion()+"-"+pedidoDetalle.getRazonSocialFacturacion());
				}
				pediodoDto.setExtensionImagen(FilenameUtils.getExtension(pedido.getRutaImagen()));
				pediodoDto.setEstadoPedido(obtenerDescripcionEstadoPedido(pedido.getIdEstadoPedido()));
				pedidosDto.add(pediodoDto);
			}catch (Exception e) {
				ExceptionUtil.controlar(e);
			}			
		});
		return pedidosDto;
	}
	
	@RequestMapping(value = "/pedido/cambiarEstado", method = RequestMethod.POST)
	public String cambiarEstadoPedido(HttpServletRequest request, Model model) {
		try {
			int id = request.getParameter("idPedido") == null ? 0 : Integer.parseInt(request.getParameter("idPedido"));
			int idEstadoPedido = Integer.parseInt(request.getParameter("idEstadoPedido"));
			Integer idUsuario = RequestUtil.parseInt(request.getParameter("idUsuario"));
			
			pedidoService.actualizarEstadoPedido(id, idEstadoPedido);
			
			if(idEstadoPedido == EstadoPedido.PAGO_CONFIRMADO.getId()){
				Pedido pedido = pedidoService.obtenerPedido(id);
				usuarioService.actualizarNumeroPedidosPagados(pedido.getIdUsuario());
			}
			
			
			model.addAttribute("mostrarNotificacionExito", true);
			
			model.addAttribute("idUsuario",idUsuario);
			
		} catch (Exception excepcion) {
			excepcion.printStackTrace();
		}
		return PAGINA_LISTAR;
	}
	
	@RequestMapping(value={"/pedido/verPdf/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<?> verPdfCarta(Model model, @PathVariable(required = true, name = "id") Integer id) {
		
		try {
			
			ArchivoRespuesta archivo = pedidoService.descargarPdf(id);
			
			HttpHeaders headers = new HttpHeaders();
	        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
	        headers.add("Pragma", "no-cache");
	        headers.add("Expires", "0");
	        headers.add("Content-Disposition", "inline; filename=" + archivo.getNombre());
			
			return ResponseEntity.ok()
					             .headers(headers)
					             .contentLength(archivo.getTamanio())
					             .contentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE))
					             .body(archivo.getByteArrayResource());
			
		} catch (Exception excepcion) {
			return ResponseEntity.badRequest().body(excepcion.getMessage());
		}
		
	}
	
	private String obtenerNombreTipoCertificado(Integer id) throws Exception{
		return tipoCertificadoService.buscarPorId(id);
	}
	
	private String obtenerNombreTiempoVigencia(Integer id) throws Exception{
		return tiempoVigenciaService.buscarPorId(id);
	}
	
	private String obtenerDescripcionEstadoPedido(int idEstadoPedido) throws Exception{
		return EstadoPedido.obtener(idEstadoPedido).getNombre();
	}
	
	private String obtenerNombreApellidoUsuario(int idUsuario) throws Exception {
		return usuarioService.obtenerNombreApellidoUsuario(idUsuario);
	}
	
	private List<EstadoPedidoDto> obtenerEstadoPedido() throws Exception{
		List<EstadoPedidoDto> estadoPedido = new ArrayList<EstadoPedidoDto>();
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.TODOS));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.EN_PROCESO));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.PENDIENTE_PAGAR));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.PAGO_CONFIRMAR));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.PAGO_CONFIRMADO));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.VALIDACION_COMPLETA));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.CERTIFICADO_EMITIDO));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.CERTIFICADO_DESCARGADO));
		estadoPedido.add(new EstadoPedidoDto(EstadoPedido.FACTURA_EMITIDA));
		return estadoPedido;
	}
	
	private List<TipoCertificadoDto> obtenerTiposCertificado() throws Exception{
		List<TipoCertificadoDto> tiposCertificados = new ArrayList<TipoCertificadoDto>();
		
		tiposCertificados.add(new TipoCertificadoDto(0, "Todos"));
		tipoCertificadoService.listarTodos()
			.forEach( (tipoCertificado) -> {
				TipoCertificadoDto tipoCertificacionDto = new TipoCertificadoDto();
				tipoCertificacionDto.setId(tipoCertificado.getId());
				tipoCertificacionDto.setNombre(tipoCertificado.getNombre());
				tiposCertificados.add(tipoCertificacionDto);
			});
		
		return tiposCertificados;
	}
}
