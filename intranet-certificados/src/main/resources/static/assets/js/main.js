$(document).ready(function(){

    // Datatables init
//    if ($('#gestion-aliados')) {
//        $('#gestion-aliados').DataTable({
//            scrollX: true,
//            dom: '<"table-header"lf>t<"table-footer"ip>'
//        });
//    }

    // Modals
    $('.modal-toggle').on('click', function() {
        var modalTarget = this.getAttribute('data-target');

        $('#overlay').addClass('show');
        document.getElementById(modalTarget).classList.add('down');

        $('.modal-close, #overlay').on('click', function(){
            $('#overlay').removeClass('show');
            document.getElementById(modalTarget).classList.remove('down');
        });
    });
    $('.modal').on('click', function(e){
        e.stopPropagation();
    });


    // Form validation
    var validation = $(this).parents('form').validate({
    	ignore: '',
    	rules: {
    		password: "required",
    	    "password_again": {
    	      equalTo: "#password"
    	    },
    	    constanciaPago: "required"
    	},
        errorLabelContainer: '.validation',
        messages: {
            required: 'Este campo es obligatorio',
            equalTo: 'Los campos deben coincidir'
        }
    });

    $('.form-submit').on('click', function(e){
        e.preventDefault();
        
        if ($(this).parents('form').valid()) {

        }
        //console.log($(this).parents('form').validate());
    });



});


var agregarDecimales = function(numero, cantidadDecimales){
	if( isNaN(numero) ){
		return "0.00";
	}
	return numero.toFixed(cantidadDecimales);
}