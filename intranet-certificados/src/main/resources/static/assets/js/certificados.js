$.fn.ploading.defaults = {
    spinnerHTML: '<i></i>',
	spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
}

var $loading = {
	mostrar : function(){
		$("body").ploading({
            action: 'show'
		});
	},
	ocultar : function(){
		$("body").ploading({
            action: 'hide'
		});
	}
}


$(window).on('load', function(){
    $('.mask').fadeOut ('fast', function ()  { $(this).remove () } )
})

$(document).on({
    ajaxStart: function() { $loading.mostrar(); },
    ajaxStop : function() { $loading.ocultar(); }
});
