$(document).ready(function(){

    // Datatables init
    if ($('#gestion-aliados')) {
        $('#gestion-aliados').DataTable({
            scrollX: true,
            dom: '<"table-header"lf>t<"table-footer"ip>'
        });
    }

    // Modals
    $('.modal-toggle').on('click', function() {
        var modalTarget = this.getAttribute('data-target');

        $('#overlay').addClass('show');
        document.getElementById(modalTarget).classList.add('down');

        $('.modal-close, #overlay').on('click', function(){
            $('#overlay').removeClass('show');
            document.getElementById(modalTarget).classList.remove('down');
        });
    });
    $('.modal').on('click', function(e){
        e.stopPropagation();
    });


    // Form validation
    var validation = $(this).parents('form').validate({
        errorLabelContainer: '.validation',
        onfocusout: function(element) { console.log($(element).valid()); },
        messages: {
            required: 'Este campo es obligatorio'
        }
    });

    $('.form-submit').on('click', function(e){
        e.preventDefault();
        
        if ($(this).parents('form').valid()) {

        }
        //console.log($(this).parents('form').validate());
    });

    

});